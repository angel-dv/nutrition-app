package com.inblexapp.nutrition_app.Data.local;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.inblexapp.nutrition_app.Entities.foods.FoodsResponse;
import com.inblexapp.nutrition_app.Entities.foods.GrupoAlimento;
import com.inblexapp.nutrition_app.Entities.foods.entity.FoodsEntity;

//@Database(entities = {FoodsEntity.class}, version = 1, exportSchema = false)
//@TypeConverters({GrupoAlimento.class})
public abstract class NutritionRoomDatabase extends RoomDatabase {

    public abstract NutritionDao getFoodDao();
}
