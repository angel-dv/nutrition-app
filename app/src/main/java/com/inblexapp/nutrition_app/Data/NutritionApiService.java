package com.inblexapp.nutrition_app.Data;

import com.inblexapp.nutrition_app.Entities.foods.FoodsResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface NutritionApiService {

    @GET("alimentos")
    Call<FoodsResponse> loadFoodCategory();
}
