package com.inblexapp.nutrition_app.Data;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.room.Room;

import com.inblexapp.nutrition_app.Data.connection.NetworkBoundResource;
import com.inblexapp.nutrition_app.Data.connection.Resource;
import com.inblexapp.nutrition_app.Data.local.NutritionDao;
import com.inblexapp.nutrition_app.Data.local.NutritionRoomDatabase;
import com.inblexapp.nutrition_app.Entities.foods.FoodsResponse;
import com.inblexapp.nutrition_app.Entities.foods.entity.FoodsEntity;
import com.inblexapp.nutrition_app.Utils.Constants;
import com.inblexapp.nutrition_app.Utils.MyApp;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NutritionRepository {
    private final NutritionApiService nutritionApiService;
//    private final NutritionDao nutritionDao;

    public NutritionRepository() {
//        NutritionRoomDatabase nutritionRoomDatabase = Room.databaseBuilder(
//                MyApp.getContext(),
//                NutritionRoomDatabase.class,
//                "db_nutrition"
//        ).build();
//        nutritionDao = nutritionRoomDatabase.getFoodDao();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        nutritionApiService = retrofit.create(NutritionApiService.class);
    }

//    public LiveData<Resource<List<FoodsEntity>>> getNutrition() {
//        return new NetworkBoundResource<List<FoodsEntity>, FoodsResponse>() {
//
//            @Override
//            protected void saveCallResult(@NonNull FoodsResponse item) {
//                nutritionDao.saveFood(item.getResults());
//            }
//
//            @NonNull
//            @Override
//            protected LiveData<List<FoodsEntity>> loadFromDb() {
//                return nutritionDao.loadFoodsCategory();
//            }
//
//            @NonNull
//            @Override
//            protected Call<FoodsResponse> createCall() {
//                return nutritionApiService.loadFoodCategory();
//            }
//        }.getAsLiveData();
//    }
}
