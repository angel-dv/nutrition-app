package com.inblexapp.nutrition_app.Data.local.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DataBase extends SQLiteOpenHelper {
    public static final String NOMBRE_BD = "Nutricion";
    public static final int VERISON_BD = 1;

    public static final String HISTORIAL = "CREATE TABLE HISTORIAL(" +
            "id INTEGER PRIMARY KEY, " +
            "fecha DATE, " +
            "puntos INTEGER)";

    public static final String GRUPOS_ALIMENTOS = "CREATE TABLE GRUPOS_ALIMENTOS(" +
            "id INTEGER PRIMARY KEY, " +
            "nombre TEXT," +
            "descripcion TEXR)";

    public static final String GRUPOS_EXCLUIDOS = "CREATE TABLE GRUPOS_EXCLUIDOS(" +
            "id INTEGER PRIMARY KEY, " +
            "grupos_alimentos_id INTEGER, " +
            "CONSTRAINT fk_ga_id FOREIGN KEY (grupos_alimentos_id) REFERENCES GRUPOS_ALIMENTOS(id))";

    public static final String ALIMENTOS = "CREATE TABLE ALIMENTOS(" +
            "id INTEGER PRIMARY KEY, " +
            "grupos_alimentos_id INTEGER, " +
            "nombre TEXT, " +
            "descripcion TEXT, " +
            "punto INTEGER, " +
            "CONSTRAINT fk_ga_id FOREIGN KEY (grupos_alimentos_id) REFERENCES GRUPOS_ALIMENTOS(id))";

    public static final String PLATILLOS = "CREATE TABLE PLATILLOS(" +
            "id DOUBLE PRIMARY KEY, " +
            "nombre TEXT, " +
            "puntos INTEGER, " +
            "instrucciones TEXT," +
            "tipo INTEGER)";

    public static final String PLATILLOS_HAS_ALIMENTOS = "CREATE TABLE PLATILLOS_HAS_ALIMENTOS(" +
            "platillos_id DOUBLE," +
            "alimentos_id INTEGER," +
            "CONSTRAINT fk_platillos_id FOREIGN KEY (platillos_id) REFERENCES PLATILLOS(id)," +
            "CONSTRAINT fk_alimentos_id FOREIGN KEY (alimentos_id) REFERENCES ALIMENTOS(id))";

    public static final String INFORMACION_NUTRIMENTAL = "CREATE TABLE INFORMACION_NUTRIMENTAL(" +
            "id DOUBLE PRIMARY KEY, " +
            "alimentos_id INTEGER, " +
            "calorias INTEGER, " +
            "carbohidratos INTEGER," +
            "proteinas INTEGER," +
            "azucares INTEGER," +
            "CONSTRAINT fk_alimento_id FOREIGN KEY (alimentos_id) REFERENCES ALIMENTOS(id))";

    public static final String ALIMENTOS_EXCLUIDOS = "CREATE TABLE ALIMENTOS_EXCLUIDOS(" +
            "id INTEGER PRIMARY KEY," +
            "alimentos_id INTEGER," +
            "CONSTRAINT fk_alimento_id FOREIGN KEY (alimentos_id) REFERENCES ALIMENTOS(id))";

    public DataBase(@Nullable Context context) {
        super(context, NOMBRE_BD, null, VERISON_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(HISTORIAL);
        sqLiteDatabase.execSQL(GRUPOS_ALIMENTOS);
        sqLiteDatabase.execSQL(GRUPOS_EXCLUIDOS);
        sqLiteDatabase.execSQL(ALIMENTOS);
        sqLiteDatabase.execSQL(PLATILLOS);
        sqLiteDatabase.execSQL(PLATILLOS_HAS_ALIMENTOS);
        sqLiteDatabase.execSQL(INFORMACION_NUTRIMENTAL);
        sqLiteDatabase.execSQL(ALIMENTOS_EXCLUIDOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS HISTORIAL");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS GRUPOS_ALIMENTOS");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS GRUPOS_EXCLUIDOS");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS ALIMENTOS");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS PLATILLOS");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS PLATILLOS_HAS_ALIMENTOS");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS INFORMACION_NUTRIMENTAL");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS ALIMENTOS_EXCLUIDOS");
        sqLiteDatabase.execSQL(HISTORIAL);
        sqLiteDatabase.execSQL(GRUPOS_ALIMENTOS);
        sqLiteDatabase.execSQL(GRUPOS_EXCLUIDOS);
        sqLiteDatabase.execSQL(ALIMENTOS);
        sqLiteDatabase.execSQL(PLATILLOS);
        sqLiteDatabase.execSQL(PLATILLOS_HAS_ALIMENTOS);
        sqLiteDatabase.execSQL(INFORMACION_NUTRIMENTAL);
        sqLiteDatabase.execSQL(ALIMENTOS_EXCLUIDOS);
    }

    public void agregarMovHistorial(int id, String fecha, int puntos){
        SQLiteDatabase bd = getWritableDatabase();
        if(bd != null){
            bd.execSQL("INSERT INTO HISTORIAL VALUES(" + id + ", " + fecha + ", " + puntos + ")");
            bd.close();
        }
    }


}
