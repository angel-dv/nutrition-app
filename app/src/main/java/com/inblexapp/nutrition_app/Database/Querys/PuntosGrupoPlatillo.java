package com.inblexapp.nutrition_app.Database.Querys;

public class PuntosGrupoPlatillo {

    private double platillo;
    private int grupo, puntos;

    public PuntosGrupoPlatillo(double platillo, int grupo, int puntos) {
        this.platillo = platillo;
        this.grupo = grupo;
        this.puntos = puntos;
    }

    public double getPlatillo() {
        return platillo;
    }

    public int getGrupo() {
        return grupo;
    }

    public int getPuntos() {
        return puntos;
    }
}
