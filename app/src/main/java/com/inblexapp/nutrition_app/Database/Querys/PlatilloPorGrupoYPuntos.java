package com.inblexapp.nutrition_app.Database.Querys;

public class PlatilloPorGrupoYPuntos {

    private double id;
    private String nombre;
    private int puntos;
    private String ruta_Imagen;

    public PlatilloPorGrupoYPuntos(double id, String nombre, int puntos, String ruta_Imagen) {
        this.id = id;
        this.nombre = nombre;
        this.puntos = puntos;
        this.ruta_Imagen = ruta_Imagen;
    }

    public double getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public int getPuntos() {
        return puntos;
    }

    public String getRuta_Imagen() {
        return ruta_Imagen;
    }
}
