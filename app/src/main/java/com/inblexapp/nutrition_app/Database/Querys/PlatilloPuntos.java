package com.inblexapp.nutrition_app.Database.Querys;

public class PlatilloPuntos {

    public double idPlatillo;
    public String nombrePlatillo;
    public int puntosPlatillo;
    public int puntosGrupo;

    public PlatilloPuntos(double idPlatillo, String nombrePlatillo, int puntosPlatillo, int puntosGrupo) {
        this.idPlatillo = idPlatillo;
        this.nombrePlatillo = nombrePlatillo;
        this.puntosPlatillo = puntosPlatillo;
        this.puntosGrupo = puntosGrupo;
    }

    public double getIdPlatillo() {
        return idPlatillo;
    }

    public String getNombrePlatillo() {
        return nombrePlatillo;
    }

    public int getPuntosPlatillo() {
        return puntosPlatillo;
    }

    public int getPuntosGrupo() {
        return puntosGrupo;
    }
}
