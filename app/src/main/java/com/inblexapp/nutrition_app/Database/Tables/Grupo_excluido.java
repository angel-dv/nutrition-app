package com.inblexapp.nutrition_app.Database.Tables;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "grupos_excluidos",foreignKeys = @ForeignKey(entity = Grupo_alimento.class,parentColumns = "id",childColumns = "grupos_alimentos_id"))
public class Grupo_excluido {

    @PrimaryKey
    private double id;

    private int grupos_alimentos_id;

    public Grupo_excluido(double id, int grupos_alimentos_id) {
        this.id = id;
        this.grupos_alimentos_id = grupos_alimentos_id;
    }

    public double getId() {
        return id;
    }

    public int getGrupos_alimentos_id() {
        return grupos_alimentos_id;
    }
}
