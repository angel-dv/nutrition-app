package com.inblexapp.nutrition_app.Database.Tables;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tipos_platillos")
public class Tipo_platillo {

    @PrimaryKey
    private double id;

    private String nombre;

    public Tipo_platillo(double id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public double getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
}
