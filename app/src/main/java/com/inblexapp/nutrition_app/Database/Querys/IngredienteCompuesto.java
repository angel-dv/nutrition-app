package com.inblexapp.nutrition_app.Database.Querys;

public class IngredienteCompuesto {

    public String nombre;
    public String cantidad;
    public String porcion;
    public int puntos;

    public IngredienteCompuesto(String nombre, String cantidad, String porcion, int puntos) {
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.porcion = porcion;
        this.puntos = puntos;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCantidad() {
        return cantidad;
    }

    public String getPorcion() {
        return porcion;
    }

    public int getPuntos() {
        return puntos;
    }
}
