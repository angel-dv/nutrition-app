package com.inblexapp.nutrition_app.Database.Tables;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "puntos_grupo", foreignKeys = {
        @ForeignKey(entity = Grupo_alimento.class, childColumns = "grupos_alimentos_id", parentColumns = "id"),
        @ForeignKey(entity = Platillo.class, childColumns = "platillos_id", parentColumns = "id")
})
public class Punto_grupo {
    @PrimaryKey
    private double id;

    private int grupos_alimentos_id;

    private double platillos_id;

    private int puntos;

    public Punto_grupo(double id, int grupos_alimentos_id, double platillos_id, int puntos) {
        this.id = id;
        this.grupos_alimentos_id = grupos_alimentos_id;
        this.platillos_id = platillos_id;
        this.puntos = puntos;
    }

    public double getId() {
        return id;
    }

    public int getGrupos_alimentos_id() {
        return grupos_alimentos_id;
    }

    public double getPlatillos_id() {
        return platillos_id;
    }

    public int getPuntos() {
        return puntos;
    }
}
