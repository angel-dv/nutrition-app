package com.inblexapp.nutrition_app.Database.Tables;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "informacion_nutrimental",foreignKeys = @ForeignKey(entity = Alimento.class,parentColumns = "id",childColumns = "alimentos_id"))
public class Informacion_nutrimental {

    @PrimaryKey
    private double id;

    private int alimentos_id;

    private int calorias;

    private int carbohidratos;

    private int proteinas;

    private int azucares;

    public Informacion_nutrimental(double id, int alimentos_id, int calorias, int carbohidratos, int proteinas, int azucares) {
        this.id = id;
        this.alimentos_id = alimentos_id;
        this.calorias = calorias;
        this.carbohidratos = carbohidratos;
        this.proteinas = proteinas;
        this.azucares = azucares;
    }

    public double getId() {
        return id;
    }

    public int getAlimentos_id() {
        return alimentos_id;
    }

    public int getCalorias() {
        return calorias;
    }

    public int getCarbohidratos() {
        return carbohidratos;
    }

    public int getProteinas() {
        return proteinas;
    }

    public int getAzucares() {
        return azucares;
    }
}
