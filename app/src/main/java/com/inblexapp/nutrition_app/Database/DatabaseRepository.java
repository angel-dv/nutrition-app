package com.inblexapp.nutrition_app.Database;

import android.app.Application;
import android.os.AsyncTask;

import com.inblexapp.nutrition_app.Database.Querys.IngredienteCompuesto;
import com.inblexapp.nutrition_app.Database.Querys.PlatilloPorGrupoYPuntos;
import com.inblexapp.nutrition_app.Database.Querys.PlatilloPuntos;
import com.inblexapp.nutrition_app.Database.Querys.PuntosGrupoPlatillo;
import com.inblexapp.nutrition_app.Database.Tables.Alimento;
import com.inblexapp.nutrition_app.Database.Tables.Grupo_alimento;
import com.inblexapp.nutrition_app.Database.Tables.Ingrediente;
import com.inblexapp.nutrition_app.Database.Tables.Platillo;
import com.inblexapp.nutrition_app.Database.Tables.Punto_grupo;
import com.inblexapp.nutrition_app.Database.Tables.Tipo_platillo;
import com.inblexapp.nutrition_app.Database.Tables.Tipo_porcion;
import com.inblexapp.nutrition_app.Entities.ExampleCategoryPointsFood.CategoriesFoodPoints;

import java.util.List;

import androidx.lifecycle.LiveData;

public class DatabaseRepository {
    private ConsultasDao consultasDao;
    private LiveData<List<Alimento>> allAlimentos;
    private LiveData<List<Alimento>> alimentosByName;
    private LiveData<List<Alimento>> alimentosByGrupo;
    private LiveData<List<Alimento>> alimentosByNameAndGrupo;
    private LiveData<Alimento> alimentoById;
    private LiveData<List<Grupo_alimento>> allGrupos;
    private LiveData<List<Tipo_porcion>> allTipoPorcion;
    private LiveData<List<Tipo_platillo>> allTipoPlatillos;
    private LiveData<List<Platillo>> allPlatillos;
    private LiveData<Platillo> platilloById;
    private LiveData<List<IngredienteCompuesto>> ingredientesByPlatillo;
    private LiveData<List<PlatilloPuntos>> platillosPuntos;
    private LiveData<List<CategoriesFoodPoints>> puntosGruposPlatillo;
    private LiveData<List<IngredienteCompuesto>> ingredientesByGrupo;
    private LiveData<List<PuntosGrupoPlatillo>> puntosPlatillo;
    private LiveData<List<PlatilloPorGrupoYPuntos>> platillosXGrupoYPuntos;

    public DatabaseRepository(Application application){
        AlimentosDatabase database = AlimentosDatabase.getInstance(application);
        consultasDao = database.consultasBD();
        allAlimentos = consultasDao.showAllAlimentos();
        allGrupos = consultasDao.getAllGrupos();
        allTipoPorcion = consultasDao.getAllTipoPorcion();
        allTipoPlatillos = consultasDao.getAllTipoPlatillo();
        allPlatillos = consultasDao.getAllPlatillos();
    }

    //Alimento
    public void insertAlimento(Alimento alimento){
        new InsertAlimentoASyncTask(consultasDao).execute(alimento);
    }

    public void updateAlimento(Alimento alimento){
        new UpdateAlimentoASyncTask(consultasDao).execute(alimento);
    }

    public void deleteAlimento(Alimento alimento){
        new DeleteAlimentoASyncTask(consultasDao).execute(alimento);
    }

    public LiveData<List<Alimento>> getAllAlimentos(){
        return allAlimentos;
    }

    public void setAlimentosByName(String name){
        alimentosByName = consultasDao.showAlimentoByName(name);
        new ShowAlimentoByNameASyncTask(consultasDao).execute(name);
    }

    public LiveData<List<Alimento>> getAlimentosByName(){
        return alimentosByName;
    }

    public void setAlimentosByGrupo(int id){
        alimentosByGrupo = consultasDao.showAlimentoByGrupo(id);
        new ShowAlimentoByGrupoASyncTask(consultasDao).execute(id);
    }

    public LiveData<List<Alimento>> getAlimentosByGrupo(){
        return alimentosByGrupo;
    }

    public void setAlimentosByNameAndGrupo(String nombre, int grupo){
        alimentosByNameAndGrupo = consultasDao.showAlimentoByNameAndGrupo(nombre,grupo);
        new ShowAlimentoByNameAndGrupoASyncTask(consultasDao).execute(nombre,String.valueOf(grupo));
    }

    public LiveData<List<Alimento>> getAlimentosByNameAndGrupo(){
        return alimentosByNameAndGrupo;
    }

    public void setAlimentoById(int id){
        alimentoById = consultasDao.showAlimentoById(id);
        new ShowAlimentoByIdASynkTask(consultasDao).execute(id);
    }

    public LiveData<Alimento> getAlimentoById(){
        return alimentoById;
    }



    //Grupo_Alimento
    public void insertGrupoAlimento(Grupo_alimento grupo_alimento){
        new InsertGrupoAlimentoASyncTask(consultasDao).execute(grupo_alimento);
    }

    public LiveData<List<Grupo_alimento>> getAllGrupos(){
        return allGrupos;
    }



    //Tipo_porcion
    public void insertTipoPorcion(Tipo_porcion tipo_porcion){
        new InsertTipoPorcionASyncTask(consultasDao).execute(tipo_porcion);
    }

    public LiveData<List<Tipo_porcion>> getAllTipoPorcion(){
        return allTipoPorcion;
    }



    //Tipo_platillo
    public void insertTipoPlatillo(Tipo_platillo tipo_platillo){
        new InsertTipoPlatilloASyncTask(consultasDao).execute(tipo_platillo);
    }

    public LiveData<List<Tipo_platillo>> getAllTipoPlatillos(){
        return allTipoPlatillos;
    }



    //Platillo
    public void insertPlatillo(Platillo platillo){
        new InsertPlatilloAsyncTask(consultasDao).execute(platillo);
    }

    public LiveData<List<Platillo>> getAllPlatillos(){
        return allPlatillos;
    }

    public void setPlatilloById(double id){
        platilloById = consultasDao.showPlatilloById(id);
        new ShowPlatilloByIdASyncTask(consultasDao).execute(id);
    }

    public LiveData<Platillo> getPlatilloById(){
        return platilloById;
    }

    public void setPlatillosPuntos(int id, int puntos){
        platillosPuntos = consultasDao.showPlatillosPuntos(id,puntos);
        new ShowPlatillosPorGruposASynkTask(consultasDao).execute(id,puntos);
    }

    public LiveData<List<PlatilloPuntos>> getPlatillosPuntos(){
        return platillosPuntos;
    }

    public void setPuntosGruposPlatillo(double id){
        puntosGruposPlatillo = consultasDao.showPuntosGrupoPlatillo(id);
        new ShowPuntosGrupoPlatilloASyncTask(consultasDao).execute(id);
    }

    public LiveData<List<CategoriesFoodPoints>> getPuntosGruposPlatillo(){
        return puntosGruposPlatillo;
    }

    public void setPuntosPlatillo(double id){
        puntosPlatillo = consultasDao.getPuntosPlatillo(id);
        new GetPuntosPlatilloAsyncTask(consultasDao).execute(id);
    }

    public LiveData<List<PuntosGrupoPlatillo>> getPuntosPlatillo(){
        return puntosPlatillo;
    }

    public void setPlatillosXGrupoYPuntos(int puntos1, int puntos2, int puntos3, int puntos4, int puntos5, int puntos6, int puntos7, int puntos8){
        platillosXGrupoYPuntos = consultasDao.getPuntosXGrupuYPuntos(puntos1, puntos2, puntos3, puntos4, puntos5, puntos6, puntos7, puntos8);
    }

    public LiveData<List<PlatilloPorGrupoYPuntos>> getPlatillosXGrupoYPuntos(){
        return platillosXGrupoYPuntos;
    }



    //Ingrediente
    public void insertIngrediente(Ingrediente ingrediente){
        new InsertIngredienteASynkTask(consultasDao).execute(ingrediente);
    }

    public void setIngredientesByPlatillo(double id){
        ingredientesByPlatillo = consultasDao.showIngredientesPlatillo(id);
        new ShowIngredienteByIdASynkTask(consultasDao).execute(id);
    }

    public LiveData<List<IngredienteCompuesto>> getIngredientesByPlatillo(){
        return ingredientesByPlatillo;
    }

    public void setIngredientesByGrupo(double idP, int idG){
        ingredientesByGrupo = consultasDao.showIngredientesGrupo(idP, idG);
    }

    public LiveData<List<IngredienteCompuesto>> getIngredientesByGrupo(){
        return ingredientesByGrupo;
    }



    //Puntos_grupo
    public void insertPuntosGrupo(Punto_grupo punto_grupo){
        new InsertPuntosGrupoASynkTask(consultasDao).execute(punto_grupo);
    }





    //ASyncTasks
    private static class InsertAlimentoASyncTask extends AsyncTask<Alimento, Void, Void>{
        private ConsultasDao consultasDao;

        private InsertAlimentoASyncTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(Alimento... alimentos) {
            consultasDao.insertAlimento(alimentos[0]);
            return null;
        }
    }

    private static class UpdateAlimentoASyncTask extends AsyncTask<Alimento, Void, Void>{
        private ConsultasDao consultasDao;

        private UpdateAlimentoASyncTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(Alimento... alimentos) {
            consultasDao.updateAlimento(alimentos[0]);
            return null;
        }
    }

    private static class DeleteAlimentoASyncTask extends AsyncTask<Alimento, Void, Void>{
        private ConsultasDao consultasDao;

        private DeleteAlimentoASyncTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(Alimento... alimentos) {
            consultasDao.deleteAlimento(alimentos[0]);
            return null;
        }
    }

    private static class ShowAlimentoByNameASyncTask extends AsyncTask<String,Void,Void>{
        private ConsultasDao consultasDao;

        public ShowAlimentoByNameASyncTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(String... strings) {
            consultasDao.showAlimentoByName(strings[0]);
            return null;
        }
    }

    private static class ShowAlimentoByGrupoASyncTask extends AsyncTask<Integer,Void,Void>{
        private ConsultasDao consultasDao;

        public ShowAlimentoByGrupoASyncTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            consultasDao.showAlimentoByGrupo(integers[0]);
            return null;
        }
    }

    private static class ShowAlimentoByNameAndGrupoASyncTask extends AsyncTask<String,String,Void>{
        private ConsultasDao consultasDao;

        public ShowAlimentoByNameAndGrupoASyncTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(String... strings) {
            consultasDao.showAlimentoByNameAndGrupo(strings[0],Integer.parseInt(strings[1]));
            return null;
        }
    }

    private static class InsertGrupoAlimentoASyncTask extends AsyncTask<Grupo_alimento,Void,Void>{
        private ConsultasDao consultasDao;

        private InsertGrupoAlimentoASyncTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(Grupo_alimento... grupo_alimentos) {
            consultasDao.insertGrupoAlimento(grupo_alimentos[0]);
            return null;
        }
    }

    private static class InsertTipoPorcionASyncTask extends AsyncTask<Tipo_porcion,Void,Void>{
        private ConsultasDao consultasDao;

        public InsertTipoPorcionASyncTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(Tipo_porcion... tipo_porcions) {
            consultasDao.insertTipoPorcion(tipo_porcions[0]);
            return null;
        }
    }

    private static class InsertTipoPlatilloASyncTask extends AsyncTask<Tipo_platillo,Void,Void>{
        private ConsultasDao consultasDao;

        public InsertTipoPlatilloASyncTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(Tipo_platillo... tipo_platillos) {
            consultasDao.insertTipoPlatillo(tipo_platillos[0]);
            return null;
        }
    }

    private static class InsertPlatilloAsyncTask extends AsyncTask<Platillo,Void,Void>{
        private ConsultasDao consultasDao;

        public InsertPlatilloAsyncTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(Platillo... platillos) {
            consultasDao.insertPlatillo(platillos[0]);
            return null;
        }
    }

    private static class ShowPlatilloByIdASyncTask extends AsyncTask<Double,Void,Void>{
        private ConsultasDao consultasDao;

        public ShowPlatilloByIdASyncTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(Double... doubles) {
            consultasDao.showPlatilloById(doubles[0]);
            return null;
        }
    }

    private static class ShowAlimentoByIdASynkTask extends AsyncTask<Integer,Void,Void>{
        private ConsultasDao consultasDao;

        public ShowAlimentoByIdASynkTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            consultasDao.showAlimentoById(integers[0]);
            return null;
        }
    }

    private static class InsertIngredienteASynkTask extends AsyncTask<Ingrediente,Void,Void>{
        private ConsultasDao consultasDao;

        public InsertIngredienteASynkTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(Ingrediente... ingredientes) {
            consultasDao.insertIngrediente(ingredientes[0]);
            return null;
        }
    }

    private static class ShowIngredienteByIdASynkTask extends AsyncTask<Double,Void,Void>{
        private ConsultasDao consultasDao;

        public ShowIngredienteByIdASynkTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(Double... doubles) {
            consultasDao.showIngredientesPlatillo(doubles[0]);
            return null;
        }
    }

    private static class InsertPuntosGrupoASynkTask extends AsyncTask<Punto_grupo,Void,Void>{
        private ConsultasDao consultasDao;

        public InsertPuntosGrupoASynkTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(Punto_grupo... punto_grupos) {
            consultasDao.insertPuntosGrupo(punto_grupos[0]);
            return null;
        }
    }

    private static class ShowPlatillosPorGruposASynkTask extends AsyncTask<Integer,Integer,Void>{
        private ConsultasDao consultasDao;

        public ShowPlatillosPorGruposASynkTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            consultasDao.showPlatillosPuntos(integers[0],integers[1]);
            return null;
        }
    }

    private static class ShowPuntosGrupoPlatilloASyncTask extends AsyncTask<Double,Void,Void>{
        private ConsultasDao consultasDao;

        public ShowPuntosGrupoPlatilloASyncTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(Double... doubles) {
            consultasDao.showPuntosGrupoPlatillo(doubles[0]);
            return null;
        }
    }

    private static class GetPuntosPlatilloAsyncTask extends AsyncTask<Double,Void,Void>{
        private ConsultasDao consultasDao;

        public GetPuntosPlatilloAsyncTask(ConsultasDao consultasDao){
            this.consultasDao = consultasDao;
        }

        @Override
        protected Void doInBackground(Double... doubles) {
            consultasDao.getPuntosPlatillo(doubles[0]);
            return null;
        }
    }

}
