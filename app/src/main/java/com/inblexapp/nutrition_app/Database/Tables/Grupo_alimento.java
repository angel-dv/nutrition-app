package com.inblexapp.nutrition_app.Database.Tables;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "grupos_alimentos")
public class Grupo_alimento {

    @PrimaryKey
    private int id;

    private String nombre;

    private String descripcion;

    public Grupo_alimento(int id, String nombre, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
