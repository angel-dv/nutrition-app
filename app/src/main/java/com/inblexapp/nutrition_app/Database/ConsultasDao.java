package com.inblexapp.nutrition_app.Database;

import com.inblexapp.nutrition_app.Database.Querys.IngredienteCompuesto;
import com.inblexapp.nutrition_app.Database.Querys.PlatilloPorGrupoYPuntos;
import com.inblexapp.nutrition_app.Database.Querys.PlatilloPuntos;
import com.inblexapp.nutrition_app.Database.Querys.PuntosGrupoPlatillo;
import com.inblexapp.nutrition_app.Database.Tables.Alimento;
import com.inblexapp.nutrition_app.Database.Tables.Grupo_alimento;
import com.inblexapp.nutrition_app.Database.Tables.Ingrediente;
import com.inblexapp.nutrition_app.Database.Tables.Platillo;
import com.inblexapp.nutrition_app.Database.Tables.Punto_grupo;
import com.inblexapp.nutrition_app.Database.Tables.Tipo_platillo;
import com.inblexapp.nutrition_app.Database.Tables.Tipo_porcion;
import com.inblexapp.nutrition_app.Entities.ExampleCategoryPointsFood.CategoriesFoodPoints;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

@Dao
public interface ConsultasDao {

    //Alimento
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAlimento(Alimento alimento);

    @Update
    void updateAlimento(Alimento alimento);

    @Delete
    void deleteAlimento(Alimento alimento);

    @Query("SELECT * FROM alimentos ORDER BY nombre ASC")
    LiveData<List<Alimento>> showAllAlimentos();

    @Query("SELECT * FROM alimentos WHERE nombre LIKE :nombre ")
    LiveData<List<Alimento>> showAlimentoByName(String nombre);

    @Query("SELECT * FROM alimentos WHERE grupos_alimentos_id = :grupo")
    LiveData<List<Alimento>> showAlimentoByGrupo(int grupo);

    @Query("SELECT * FROM alimentos WHERE nombre LIKE :nombre AND grupos_alimentos_id = :grupo")
    LiveData<List<Alimento>> showAlimentoByNameAndGrupo(String nombre, int grupo);

    @Query("SELECT * FROM alimentos WHERE puntos = :puntos")
    LiveData<List<Alimento>> showAlimentosByPoints(int puntos);

    @Query("SELECT * FROM alimentos WHERE grupos_alimentos_id = :id")
    LiveData<Alimento> showAlimentosByGroup(int id);

    @Query("SELECT * FROM alimentos WHERE id = :id")
    LiveData<Alimento> showAlimentoById(int id);



    //Grupo_alimento
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertGrupoAlimento(Grupo_alimento grupo);

    @Update
    void updateGrupoAlimento(Grupo_alimento grupo);

    @Delete
    void deleteGrupoAlimento(Grupo_alimento grupo);

    @Query("SELECT * FROM grupos_alimentos ORDER BY id DESC")
    LiveData<List<Grupo_alimento>> getAllGrupos();



    //Tipo_porcion
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTipoPorcion(Tipo_porcion tipo_porcion);

    @Update
    void updateTipoPorcion(Tipo_porcion tipo_porcion);

    @Delete
    void deleteTipoPorcion(Tipo_porcion tipo_porcion);

    @Query("SELECT * FROM tipos_porciones ORDER BY id DESC")
    LiveData<List<Tipo_porcion>> getAllTipoPorcion();



    //Tipo_platillo
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTipoPlatillo(Tipo_platillo tipo_platillo);

    @Update
    void updateTipoPlatillo(Tipo_platillo tipo_platillo);

    @Delete
    void deleteTipoPlatillo(Tipo_platillo tipo_platillo);

    @Query("SELECT * FROM tipos_platillos")
    LiveData<List<Tipo_platillo>> getAllTipoPlatillo();



    //Platillo
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPlatillo(Platillo platillo);

    @Update
    void updatePlatillo(Platillo platillo);

    @Delete
    void deletePlatillo(Platillo platillo);

    @Query("SELECT * FROM platillos")
    LiveData<List<Platillo>> getAllPlatillos();

    @Query("SELECT * FROM platillos WHERE id = :id")
    LiveData<Platillo> showPlatilloById(double id);

    @Transaction
    @Query("select " +
            "ga.id as id, " +
            "ga.nombre as title, " +
            "pg.puntos as points, " +
            "ga.id as color " +
            "from puntos_grupo pg " +
            "inner join grupos_alimentos ga on pg.grupos_alimentos_id = ga.id " +
            "where pg.platillos_id = :id")
    LiveData<List<CategoriesFoodPoints>> showPuntosGrupoPlatillo(double id);

    @Query("select " +
            "platillos_id as platillo, " +
            "grupos_alimentos_id as grupo, " +
            "puntos as puntos " +
            "from puntos_grupo " +
            "where platillos_id = :id")
    LiveData<List<PuntosGrupoPlatillo>> getPuntosPlatillo(double id);

    @Transaction
    @Query("select distinct " +
            "p.id as id, " +
            "p.nombre as nombre, " +
            "p.puntos as puntos, " +
            "p.ruta_imagen as ruta_imagen " +
            "from platillos p " +
            "join puntos_grupo pg on p.id = pg.platillos_id " +
            "where (pg.grupos_alimentos_id = 1 and pg.puntos <= :puntos1) " +
            "or (pg.grupos_alimentos_id = 2 and pg.puntos <= :puntos2) " +
            "or (pg.grupos_alimentos_id = 3 and pg.puntos <= :puntos3) " +
            "or (pg.grupos_alimentos_id = 4 and pg.puntos <= :puntos4) " +
            "or (pg.grupos_alimentos_id = 5 and pg.puntos <= :puntos5) " +
            "or (pg.grupos_alimentos_id = 6 and pg.puntos <= :puntos6) " +
            "or (pg.grupos_alimentos_id = 7 and pg.puntos <= :puntos7) " +
            "or (pg.grupos_alimentos_id = 8 and pg.puntos <= :puntos8) ;")
    LiveData<List<PlatilloPorGrupoYPuntos>> getPuntosXGrupuYPuntos(int puntos1, int puntos2, int puntos3, int puntos4, int puntos5, int puntos6, int puntos7, int puntos8);



    //Ingredientes
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertIngrediente(Ingrediente ingrediente);

    @Transaction
    @Query("SELECT " +
            "a.nombre AS nombre, " +
            "i.cad_cantidad AS cantidad, " +
            "tp.abreviatura AS porcion, " +
            "i.puntos As puntos" +
            " FROM ingredientes i " +
            "INNER JOIN alimentos a ON i.alimentos_id = a.id " +
            "INNER JOIN tipos_porciones tp ON i.tipos_porciones_id = tp.id " +
            "WHERE i.platillos_id = :id")
    LiveData<List<IngredienteCompuesto>> showIngredientesPlatillo(double id);

    @Transaction
    @Query("select " +
            "a.nombre as nombre, " +
            "i.cad_cantidad as cantidad, " +
            "tp.abreviatura as porcion, " +
            "i.puntos as puntos " +
            "from ingredientes i " +
            "inner join platillos p on i.platillos_id = p.id " +
            "inner join alimentos a on i.alimentos_id = a.id " +
            "inner join tipos_porciones tp on i.tipos_porciones_id = tp.id " +
            "where p.id = :idP " +
            "and a.grupos_alimentos_id = :idG")
    LiveData<List<IngredienteCompuesto>> showIngredientesGrupo(double idP, int idG);



    //Punto_grupo
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPuntosGrupo(Punto_grupo punto_grupo);

    @Transaction
    @Query("select " +
            "p.id as idPlatillo, " +
            "p.nombre as nombrePlatillo, " +
            "p.puntos as puntosPlatillo, " +
            "pg.puntos as puntosGrupo " +
            "from platillos p " +
            "join puntos_grupo pg on p.id = pg.platillos_id " +
            "where pg.grupos_alimentos_id = :id " +
            "and pg.puntos <= :puntos")
    LiveData<List<PlatilloPuntos>> showPlatillosPuntos(int id, int puntos);


    /*

    //Informacion_nutrimental
    @Insert
    void insertInformacionNutrimental(Informacion_nutrimental informacion);

    @Update
    void updateInformacionNutrimental(Informacion_nutrimental informacion);

    @Delete
    void deleteInformacionNutrimental(Informacion_nutrimental informacion);

    //@Query("SELECT * FROM informaciones_nutrimentales")
    //LiveData<List<Informacion_nutrimental>> getAllInformacionN();

    @Query("SELECT * FROM informaciones_nutrimentales WHERE alimento_id = :id")
    LiveData<Informacion_nutrimental> getInfNutrAlimento(int id);

    @Query("SELECT * FROM platillos WHERE id = :id")
    LiveData<Platillo> getOnePlatillo(int id);

    

    //Platillo_alimento
    @Insert
    void insertPlatilloAlimento(Platillo_alimento platillo_alimento);

    @Update
    void updatePlatilloAlimento(Platillo_alimento platillo_alimento);

    @Delete
    void deletePlatilloAlimento(Platillo_alimento platillo_alimento);

    @Query("SELECT * FROM platillos_alimentos pa JOIN platillos p ON pa.alimento_id = p.id WHERE pa.alimento_id = :id")
    LiveData<Platillo> getOnePlatilloWithAlim(int id);

 */
}
