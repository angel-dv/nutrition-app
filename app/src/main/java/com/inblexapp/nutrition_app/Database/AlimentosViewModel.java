package com.inblexapp.nutrition_app.Database;

import android.app.Application;

import com.inblexapp.nutrition_app.Database.Querys.IngredienteCompuesto;
import com.inblexapp.nutrition_app.Database.Querys.PlatilloPorGrupoYPuntos;
import com.inblexapp.nutrition_app.Database.Querys.PlatilloPuntos;
import com.inblexapp.nutrition_app.Database.Querys.PuntosGrupoPlatillo;
import com.inblexapp.nutrition_app.Database.Tables.Alimento;
import com.inblexapp.nutrition_app.Database.Tables.Grupo_alimento;
import com.inblexapp.nutrition_app.Database.Tables.Ingrediente;
import com.inblexapp.nutrition_app.Database.Tables.Platillo;
import com.inblexapp.nutrition_app.Database.Tables.Punto_grupo;
import com.inblexapp.nutrition_app.Database.Tables.Tipo_platillo;
import com.inblexapp.nutrition_app.Database.Tables.Tipo_porcion;
import com.inblexapp.nutrition_app.Entities.ExampleCategoryPointsFood.CategoriesFoodPoints;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class AlimentosViewModel extends AndroidViewModel {

    private DatabaseRepository repository;
    private LiveData<List<Alimento>> allAlimentos;
    private LiveData<List<Alimento>> alimentosByName;
    private LiveData<List<Alimento>> alimentosByGrupo;
    private LiveData<List<Alimento>> alimentosByNameAndGrupo;
    private LiveData<Alimento> alimentoById;
    private LiveData<List<Grupo_alimento>> allGruposAlimentos;
    private LiveData<List<Tipo_porcion>> allTipoPorcion;
    private LiveData<List<Tipo_platillo>> allTipoPlatillos;
    private LiveData<List<Platillo>> allPlatillos;
    private LiveData<Platillo> platilloById;
    private LiveData<List<IngredienteCompuesto>> ingredientesPlatillo;
    private LiveData<List<PlatilloPuntos>> platillosPuntos;
    private LiveData<List<CategoriesFoodPoints>> puntosGruposPlatillo;
    private LiveData<List<IngredienteCompuesto>> ingredientesByGrupo;
    private LiveData<List<PuntosGrupoPlatillo>> puntosPlatillo;
    private LiveData<List<PlatilloPorGrupoYPuntos>> platillosXGrupoYPuntos;


    public AlimentosViewModel(@NonNull Application application) {
        super(application);
        repository = new DatabaseRepository(application);
        allAlimentos = repository.getAllAlimentos();
        allGruposAlimentos = repository.getAllGrupos();
        allTipoPorcion = repository.getAllTipoPorcion();
        allTipoPlatillos = repository.getAllTipoPlatillos();
        allPlatillos = repository.getAllPlatillos();
    }


    //Alimento
    public void insertAlimento(Alimento alimento) {
        repository.insertAlimento(alimento);
    }

    public void updateAlimento(Alimento alimento) {
        repository.updateAlimento(alimento);
    }

    public void deleteAlimento(Alimento alimento) {
        repository.deleteAlimento(alimento);
    }

    public LiveData<List<Alimento>> getAllAlimentos() {
        return allAlimentos;
    }

    public void setAlimentosByName(String name) {
        repository.setAlimentosByName(name);
        alimentosByName = repository.getAlimentosByName();
    }

    public LiveData<List<Alimento>> getAlimentosByName() {
        return alimentosByName;
    }

    public void setAlimentosByGrupo(int grupo) {
        repository.setAlimentosByGrupo(grupo);
        alimentosByGrupo = repository.getAlimentosByGrupo();
    }

    public LiveData<List<Alimento>> getAlimentosByGrupo() {
        return alimentosByGrupo;
    }

    public void setAlimentosByNameAndGrupo(String nombre, int grupo) {
        repository.setAlimentosByNameAndGrupo(nombre, grupo);
        alimentosByNameAndGrupo = repository.getAlimentosByNameAndGrupo();
    }

    public LiveData<List<Alimento>> getAlimentosByNameAndGrupo() {
        return alimentosByNameAndGrupo;
    }

    public void setAlimentoById(int id) {
        repository.setAlimentoById(id);
        alimentoById = repository.getAlimentoById();
    }

    public LiveData<Alimento> getAlimentoById() {
        return alimentoById;
    }


    //GrupoAlimento
    public void insertGrupoAlimento(Grupo_alimento grupo_alimento) {
        repository.insertGrupoAlimento(grupo_alimento);
    }

    public LiveData<List<Grupo_alimento>> getAllGruposAlimentos() {
        return allGruposAlimentos;
    }


    //TipoPorcion
    public void insertTipoPorcion(Tipo_porcion tipo_porcion) {
        repository.insertTipoPorcion(tipo_porcion);
    }

    public LiveData<List<Tipo_porcion>> getAllTipoPorcion() {
        return allTipoPorcion;
    }


    //TipoPlatillo
    public void insertTipoPlatillo(Tipo_platillo tipo_platillo) {
        repository.insertTipoPlatillo(tipo_platillo);
    }

    public LiveData<List<Tipo_platillo>> getAllTipoPlatillos() {
        return allTipoPlatillos;
    }


    //Platillos
    public void insertPlatillo(Platillo platillo) {
        repository.insertPlatillo(platillo);
    }

    public LiveData<List<Platillo>> getAllPlatillos() {
        return allPlatillos;
    }

    public void setPlatilloById(double id) {
        repository.setPlatilloById(id);
        platilloById = repository.getPlatilloById();
    }

    public LiveData<Platillo> getPlatilloById() {
        return platilloById;
    }

    public void setPlatillosPuntos(int id, int puntos) {
        repository.setPlatillosPuntos(id, puntos);
        platillosPuntos = repository.getPlatillosPuntos();
    }

    public LiveData<List<PlatilloPuntos>> getPlatillosPuntos() {
        return platillosPuntos;
    }

    public void setPuntosGruposPlatillo(double id) {
        repository.setPuntosGruposPlatillo(id);
        puntosGruposPlatillo = repository.getPuntosGruposPlatillo();
    }

    public LiveData<List<CategoriesFoodPoints>> getPuntosGruposPlatillo() {
        return puntosGruposPlatillo;
    }

    public void setPuntosPlatillo(double id) {
        repository.setPuntosPlatillo(id);
        puntosPlatillo = repository.getPuntosPlatillo();
    }

    public LiveData<List<PuntosGrupoPlatillo>> getPuntosPlatillo() {
        return puntosPlatillo;
    }

    public void setPlatillosXGrupoYPuntos(int puntos1, int puntos2, int puntos3, int puntos4, int puntos5, int puntos6, int puntos7, int puntos8){
        repository.setPlatillosXGrupoYPuntos(puntos1, puntos2, puntos3, puntos4, puntos5, puntos6, puntos7, puntos8);
        platillosXGrupoYPuntos = repository.getPlatillosXGrupoYPuntos();
    }

    public LiveData<List<PlatilloPorGrupoYPuntos>> getPlatillosXGrupoYPuntos(){
        return platillosXGrupoYPuntos;
    }


    //Ingrediente
    public void insertIngrediente(Ingrediente ingrediente) {
        repository.insertIngrediente(ingrediente);
    }

    public void setIngredientesPlatillo(double id) {
        repository.setIngredientesByPlatillo(id);
        ingredientesPlatillo = repository.getIngredientesByPlatillo();
    }

    public LiveData<List<IngredienteCompuesto>> getIngredientesPlatillo() {
        return ingredientesPlatillo;
    }

    public void setIngredientesByGrupo(double idP, int idG) {
        repository.setIngredientesByGrupo(idP, idG);
        ingredientesByGrupo = repository.getIngredientesByGrupo();
    }

    public LiveData<List<IngredienteCompuesto>> getIngredientesByGrupo() {
        return ingredientesByGrupo;
    }


    //Puntos_grupo
    public void insertPuntosGrupo(Punto_grupo punto_grupo) {
        repository.insertPuntosGrupo(punto_grupo);
    }

}
