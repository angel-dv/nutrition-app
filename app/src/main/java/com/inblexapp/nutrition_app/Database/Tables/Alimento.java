package com.inblexapp.nutrition_app.Database.Tables;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "alimentos",foreignKeys = {
        @ForeignKey(entity = Tipo_porcion.class,parentColumns = "id",childColumns = "tipos_porciones_id"),
        @ForeignKey(entity = Grupo_alimento.class,parentColumns = "id",childColumns = "grupos_alimentos_id")
})
public class Alimento {

    @PrimaryKey
    private int id;

    private double tipos_porciones_id;

    private int grupos_alimentos_id;

    private String nombre;

    private String descripcion;

    private int puntos;

    private String ruta_imagen;

    private float cantidad;

    private String cad_cantidad;

    public Alimento(int id, double tipos_porciones_id, int grupos_alimentos_id, String nombre, String descripcion, int puntos, String ruta_imagen, float cantidad, String cad_cantidad) {
        this.id = id;
        this.tipos_porciones_id = tipos_porciones_id;
        this.grupos_alimentos_id = grupos_alimentos_id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.puntos = puntos;
        this.ruta_imagen = ruta_imagen;
        this.cantidad = cantidad;
        this.cad_cantidad = cad_cantidad;
    }

    public int getId() {
        return id;
    }

    public double getTipos_porciones_id() {
        return tipos_porciones_id;
    }

    public int getGrupos_alimentos_id() {
        return grupos_alimentos_id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getPuntos() {
        return puntos;
    }

    public String getRuta_imagen() {
        return ruta_imagen;
    }

    public float getCantidad() {
        return cantidad;
    }

    public String getCad_cantidad() {
        return cad_cantidad;
    }
}
