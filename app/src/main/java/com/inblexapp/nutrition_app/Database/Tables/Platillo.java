package com.inblexapp.nutrition_app.Database.Tables;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "platillos",foreignKeys = @ForeignKey(entity = Tipo_platillo.class,parentColumns = "id",childColumns = "tipos_platillos_id"))
public class Platillo {

    @PrimaryKey
    private double id;

    private double tipos_platillos_id;

    private String nombre;

    private int puntos;

    private String instrucciones;

    private int tipo;

    private String ruta_imagen;

    public Platillo(double id, double tipos_platillos_id, String nombre, int puntos, String instrucciones, int tipo, String ruta_imagen) {
        this.id = id;
        this.tipos_platillos_id = tipos_platillos_id;
        this.nombre = nombre;
        this.puntos = puntos;
        this.instrucciones = instrucciones;
        this.tipo = tipo;
        this.ruta_imagen = ruta_imagen;
    }

    public double getId() {
        return id;
    }

    public double getTipos_platillos_id() {
        return tipos_platillos_id;
    }

    public String getNombre() {
        return nombre;
    }

    public int getPuntos() {
        return puntos;
    }

    public String getInstrucciones() {
        return instrucciones;
    }

    public int getTipo() {
        return tipo;
    }

    public String getRuta_imagen() {
        return ruta_imagen;
    }
}
