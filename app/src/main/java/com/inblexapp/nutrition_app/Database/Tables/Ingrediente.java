package com.inblexapp.nutrition_app.Database.Tables;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "ingredientes",foreignKeys = {
        @ForeignKey(entity = Tipo_porcion.class,parentColumns = "id",childColumns = "tipos_porciones_id"),
        @ForeignKey(entity = Platillo.class,parentColumns = "id",childColumns = "platillos_id"),
        @ForeignKey(entity = Alimento.class,parentColumns = "id",childColumns = "alimentos_id")
})
public class Ingrediente {

    @PrimaryKey
    private double id;

    private int alimentos_id;

    private double tipos_porciones_id;

    private double platillos_id;

    private int puntos;

    private float cantidad;

    private String cad_cantidad;

    public Ingrediente(double id, int alimentos_id, double tipos_porciones_id, double platillos_id, int puntos, float cantidad, String cad_cantidad) {
        this.id = id;
        this.alimentos_id = alimentos_id;
        this.tipos_porciones_id = tipos_porciones_id;
        this.platillos_id = platillos_id;
        this.puntos = puntos;
        this.cantidad = cantidad;
        this.cad_cantidad = cad_cantidad;
    }

    public double getId() {
        return id;
    }

    public int getAlimentos_id() {
        return alimentos_id;
    }

    public double getTipos_porciones_id() {
        return tipos_porciones_id;
    }

    public double getPlatillos_id() {
        return platillos_id;
    }

    public int getPuntos() {
        return puntos;
    }

    public float getCantidad() {
        return cantidad;
    }

    public String getCad_cantidad() {
        return cad_cantidad;
    }
}
