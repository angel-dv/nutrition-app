package com.inblexapp.nutrition_app.Database;

import android.content.Context;
import android.os.AsyncTask;

import com.inblexapp.nutrition_app.Database.Tables.Alimento;
import com.inblexapp.nutrition_app.Database.Tables.Alimento_excluido;
import com.inblexapp.nutrition_app.Database.Tables.Grupo_alimento;
import com.inblexapp.nutrition_app.Database.Tables.Grupo_excluido;
import com.inblexapp.nutrition_app.Database.Tables.Informacion_nutrimental;
import com.inblexapp.nutrition_app.Database.Tables.Ingrediente;
import com.inblexapp.nutrition_app.Database.Tables.Platillo;
import com.inblexapp.nutrition_app.Database.Tables.Punto_grupo;
import com.inblexapp.nutrition_app.Database.Tables.Tipo_platillo;
import com.inblexapp.nutrition_app.Database.Tables.Tipo_porcion;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {
        Alimento.class,
        Grupo_alimento.class,
        Tipo_porcion.class,
        Platillo.class,
        Tipo_platillo.class,
        Informacion_nutrimental.class,
        Ingrediente.class,
        Alimento_excluido.class,
        Grupo_excluido.class,
        Punto_grupo.class
    }, version = 7)
public abstract class AlimentosDatabase extends RoomDatabase {

    private static AlimentosDatabase instance;

    public abstract ConsultasDao consultasBD();

    public static synchronized AlimentosDatabase getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),AlimentosDatabase.class,"alimentos_database")
                    .fallbackToDestructiveMigration().addCallback(roomCallback).build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new EjemplosBDAsyncTask(instance).execute();
        }
    };

    private static class EjemplosBDAsyncTask extends AsyncTask<Void, Void, Void>{
        private ConsultasDao consultasDao;

        private EjemplosBDAsyncTask(AlimentosDatabase db){
            consultasDao = db.consultasBD();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            consultasDao.insertGrupoAlimento(new Grupo_alimento(1,"Frutas",""));
            consultasDao.insertGrupoAlimento(new Grupo_alimento(2,"Verduras",""));
            consultasDao.insertGrupoAlimento(new Grupo_alimento(3,"Cereales",""));
            consultasDao.insertGrupoAlimento(new Grupo_alimento(4,"Leguminosas",""));
            consultasDao.insertGrupoAlimento(new Grupo_alimento(5,"Carnes",""));
            consultasDao.insertGrupoAlimento(new Grupo_alimento(6,"Lacteos",""));
            consultasDao.insertGrupoAlimento(new Grupo_alimento(7,"Grasas",""));
            consultasDao.insertGrupoAlimento(new Grupo_alimento(8,"Libres",""));


            consultasDao.insertTipoPorcion(new Tipo_porcion(1,1,"Pizca","Pizca"));
            consultasDao.insertTipoPorcion(new Tipo_porcion(2,1,"Cucharadita","Cdta"));
            consultasDao.insertTipoPorcion(new Tipo_porcion(3,1,"Cucharada","Cda"));
            consultasDao.insertTipoPorcion(new Tipo_porcion(4,1,"Taza","Tza"));
            consultasDao.insertTipoPorcion(new Tipo_porcion(5,1,"Kilogramo","Kg"));
            consultasDao.insertTipoPorcion(new Tipo_porcion(6,1,"Gramo","g"));
            consultasDao.insertTipoPorcion(new Tipo_porcion(7,1,"Litro","l"));
            consultasDao.insertTipoPorcion(new Tipo_porcion(8,1,"Mililitro","ml"));
            consultasDao.insertTipoPorcion(new Tipo_porcion(9,1,"Pieza","Pza"));
            consultasDao.insertTipoPorcion(new Tipo_porcion(10,1,"Al gusto","Al gusto"));


            consultasDao.insertAlimento(new Alimento(1,9,1,"Ciruela","",10,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(2,9,1,"Chabacano","",7,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(3,9,1,"Durazno","",15,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(4,9,1,"Granada","",15,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(5,9,1,"Guayaba","",15,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(6,9,1,"Higo","",10,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(7,9,1,"Kiwi","",20,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(8,9,1,"Lima","",10,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(9,9,1,"Mandarina","",15,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(10,9,1,"Mango","",15,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(11,9,1,"Manzana","",30,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(12,9,1,"Naranja","",15,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(13,9,1,"Nectarina","",30,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(14,9,1,"Pasa","",3,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(15,9,1,"Pera","",30,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(16,9,1,"Platano","",60,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(17,9,1,"Tejocote","",30,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(18,9,1,"Toronja","",15,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(19,9,1,"Tuna","",10,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(20,9,1,"Uva","",2,"",1,"1"));

            consultasDao.insertAlimento(new Alimento(21,4,2,"Apio","",15,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(22,4,2,"Brócoli","",15,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(23,9,2,"Calabacita","",15,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(24,4,2,"Col","",30,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(25,9,2,"Chayote","",30,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(26,3,2,"Chicharo","",5,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(27,9,2,"Esparrago","",3,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(28,4,2,"Espinaca","",7,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(29,4,2,"Jícama","",30,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(30,9,2,"Jitomate","",4,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(31,4,2,"Lechuga","",7,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(32,4,2,"Nopal","",15,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(33,4,2,"Pepino","",5,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(34,4,2,"Rábano","",8,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(35,9,2,"Pimiento","",15,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(36,4,2,"Zanahoria","",7,"",1,"1"));

            consultasDao.insertAlimento(new Alimento(37,4,3,"Arroz","",15,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(38,4,3,"Avena integral","",105,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(39,9,3,"Barrita de avena","",70,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(40,9,3,"Bolillo integral","",105,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(41,9,3,"Bollo de hamburguesa","",70,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(42,9,3,"Camote","",140,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(43,3,3,"Centeno","",7,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(44,4,3,"Cereal integral","",105,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(45,9,3,"Elote","",30,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(46,4,3,"Pasta para sopa","",70,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(47,9,3,"Galleta dulce","",15,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(48,9,3,"Galleta integral","",11,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(49,9,3,"Galleta salada integral","",9,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(50,4,3,"Germen de trigo","",140,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(51,3,3,"Granola baja en grasa","",88,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(52,9,3,"Pan de hot-dog","",70,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(53,4,3,"Palomitas naturales","",15,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(54,9,3,"Pan blanco rebanado","",35,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(55,9,3,"Pan tostado rebanado","",30,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(56,9,3,"Papa","",23,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(57,9,3,"Tortilla de maíz","",35,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(58,9,3,"Tortilla integral","",35,"",1,"1"));

            consultasDao.insertAlimento(new Alimento(59,4,4,"Alubia","",120,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(60,4,4,"Frijol","",120,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(61,4,4,"Garbanzo","",120,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(62,4,4,"Haba","",240,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(63,4,4,"Lentejas","",120,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(64,4,4,"Soya","",180,"",1,"1"));

            consultasDao.insertAlimento(new Alimento(65,6,5,"Atún en agua","",40,"",35,"35"));
            consultasDao.insertAlimento(new Alimento(66,6,5,"Bacaláo","",40,"",45,"45"));
            consultasDao.insertAlimento(new Alimento(67,6,5,"Bistec","",40,"",35,"35"));
            consultasDao.insertAlimento(new Alimento(68,6,5,"Carne molida","",40,"",35,"35"));
            consultasDao.insertAlimento(new Alimento(69,9,5,"Huevo entero","",30,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(70,9,5,"Clara de huevo","",20,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(71,9,5,"Chuleta asada","",80,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(72,6,5,"Filete de pescado","",40,"",45,"45"));
            consultasDao.insertAlimento(new Alimento(73,9,5,"Jamón","",20,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(74,6,5,"Filete de pechuga","",40,"",40,"40"));
            consultasDao.insertAlimento(new Alimento(75,9,5,"Mojarra","",120,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(76,6,5,"Pollo molido","",40,"",40,"40"));
            consultasDao.insertAlimento(new Alimento(77,9,5,"Muslo de pollo","",120,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(78,9,5,"Pechuga de pollo","",80,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(79,6,5,"Sardina","",40,"",40,"40"));
            consultasDao.insertAlimento(new Alimento(80,6,5,"Filete de jamón","",40,"",30,"30"));
            consultasDao.insertAlimento(new Alimento(81,6,5,"Sierra","",40,"",50,"50"));

            consultasDao.insertAlimento(new Alimento(82,4,6,"Leche descremada","",50,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(83,3,6,"Leche descremada en polvo","",138,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(84,4,6,"Leche de soya","",50,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(85,3,6,"Queso cottage","",14,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(86,6,6,"Queso de cabra","",40,"",20,"20"));
            consultasDao.insertAlimento(new Alimento(87,6,6,"Queso panela","",40,"",40,"40"));
            consultasDao.insertAlimento(new Alimento(88,6,6,"Queso fresco","",40,"",40,"40"));
            consultasDao.insertAlimento(new Alimento(89,6,6,"Queso Oaxaca","",40,"",40,"40"));
            consultasDao.insertAlimento(new Alimento(90,3,6,"Requesón","",14,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(91,4,6,"Yogurt light","",60,"",1,"1"));

            consultasDao.insertAlimento(new Alimento(92,3,7,"Aceite","",200,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(93,3,7,"Aceite de canola","",200,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(94,3,7,"Aceite de girasol","",200,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(95,3,7,"Aceite de maíz","",200,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(96,3,7,"Aceite de oliva","",200,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(97,9,7,"Aceituna negra","",5,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(98,3,7,"Aderezo","",98,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(99,4,7,"Aguacate","",9,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(100,9,7,"Nuez","",6,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(101,9,7,"Almendra","",3,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(102,9,7,"Pistache","",7,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(103,6,7,"Cacahuate","",25,"",20,"20"));
            consultasDao.insertAlimento(new Alimento(104,3,7,"Pepita con cascara","",12,"",1,"1"));

            consultasDao.insertAlimento(new Alimento(105,4,8,"Agua natural","",0,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(106,9,8,"Ajo","",0,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(107,3,8,"Café descafeinado","",0,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(108,3,8,"Canela","",0,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(109,3,8,"Clavo","",0,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(110,9,8,"Cebolla","",0,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(111,9,8,"Chile","",0,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(112,4,8,"Epazote","",0,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(113,4,8,"Laurel","",0,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(114,9,8,"Limón","",0,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(115,4,8,"Hierbabuena","",0,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(116,4,8,"Manzanilla","",0,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(117,3,8,"Mostaza","",0,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(118,4,8,"Orégano","",0,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(119,4,8,"Perejil","",0,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(120,4,8,"Alcaparraz","",0,"",1,"1"));
            consultasDao.insertAlimento(new Alimento(121,4,8,"Anis","",0,"",1,"1"));

            consultasDao.insertTipoPlatillo(new Tipo_platillo(1,"Comida"));

            consultasDao.insertPlatillo(new Platillo(1,1,"Ensalada de bacalao",149,"Paso 1:\nCalentar el horno a 200°c, coloque el pescado " +
                    "en un recipiente poco profundo y vierta un poco de jugo de limon sobre el pescado (tambien puede condimentar con un poco de pimienta negra). Hornee durante 8 minutos.\n" +
                    "\nPaso 2:\nMientras tanto, ponga el resto de los ingredientes, más un poco de jugo de limon si asi lo desea, en un bol y combinelos bien. vacielo en un plato y coloque " +
                    "el bacalao encima, asegurese de cubrirlo bien entre los ingredientes.",1,""));
            consultasDao.insertPlatillo(new Platillo(2,1,"Pollo con requeson",137,"Paso 1:\nHaga un corte profundo a lo largo de la pechuga, ponga " +
                    "una cucharada de requeson dentro, posteriormente con la ayudad de un palillo de dientes cierre la apertura de la pechuga\n\nPaso 2:\nColoque la pechuga de pollo sobre una bandeja " +
                    "para asar barnizada con una cucharadita de aceite de oliva (tambien puede condimentar con un poco de pimienta negra). Cocine a temperatura media durante 8 minutos.\n\nPaso 3:\n" +
                    " Mientras tanto corte la cebolla, el tomate y el ajo y combinelos bien, finalmente incluya la pechuga al plato y combinelo bien entre los ingredientes",1,""));
            consultasDao.insertPlatillo(new Platillo(3,1,"Arroz frito",136,"Paso 1:\nPonga el arroz en una cacerola y agreguele 200 ml de agua hirviendo, " +
                    "cocine a fuego lento durante 12 minutos.\n\nPaso 2:\nMientras tanto pique las verduras y corte el jamon en trozitos, fria el jamon en una sarten barnizada con aceite de oliva y " +
                    "agregue los demas ingredientes, cocine a fuego medio, despues vierta un huevo batido a la sarten inclinandola un poco para formar un omelette delgado.\n\nPaso 3:\n" +
                    "Vacie el arroz cocido a la sarten, corte en tiras el omelette, mezcle bien los ingredientes y fria durante 5 minutos.",1,""));


            consultasDao.insertIngrediente(new Ingrediente(1,66,9,1,80,2,"2"));
            consultasDao.insertIngrediente(new Ingrediente(2,114,10,1,0,0,""));
            consultasDao.insertIngrediente(new Ingrediente(3,10,9,1,15,1,"1"));
            consultasDao.insertIngrediente(new Ingrediente(4,99,9,1,25,0.33F,"1/3"));
            consultasDao.insertIngrediente(new Ingrediente(5,33,9,1,10,0.33F,"1/3"));
            consultasDao.insertIngrediente(new Ingrediente(6,30,9,1,4,1,"1"));
            consultasDao.insertIngrediente(new Ingrediente(7,110,10,1,0,0,""));
            consultasDao.insertIngrediente(new Ingrediente(8,31,9,1,15,0.20F,"1/5"));

            consultasDao.insertIngrediente(new Ingrediente(9,78,9,2,80,1,"1"));
            consultasDao.insertIngrediente(new Ingrediente(10,90,3,2,10,1,"1"));
            consultasDao.insertIngrediente(new Ingrediente(11,96,2,2,25,1,"1"));
            consultasDao.insertIngrediente(new Ingrediente(12,30,9,2,8,1,"1"));
            consultasDao.insertIngrediente(new Ingrediente(13,110,9,2,10,0.33F,"1/3"));
            consultasDao.insertIngrediente(new Ingrediente(14,30,9,2,4,1,"1"));
            consultasDao.insertIngrediente(new Ingrediente(15,106,9,2,0,.25F,"1/4"));

            consultasDao.insertIngrediente(new Ingrediente(16,37,4,3,35,0.5F,"1/2"));
            consultasDao.insertIngrediente(new Ingrediente(17,96,2,3,25,1,"1"));
            consultasDao.insertIngrediente(new Ingrediente(18,69,9,3,30,1,"1"));
            consultasDao.insertIngrediente(new Ingrediente(19,76,9,3,20,1,"1"));
            consultasDao.insertIngrediente(new Ingrediente(20,36,9,3,6,1,"1"));
            consultasDao.insertIngrediente(new Ingrediente(21,110,9,3,0,0.33F,"1/3"));
            consultasDao.insertIngrediente(new Ingrediente(22,106,9,3,0,0.25F,"1/4"));
            consultasDao.insertIngrediente(new Ingrediente(23,26,3,3,20,4,"4"));

            consultasDao.insertPuntosGrupo(new Punto_grupo(1,1,1,45));
            consultasDao.insertPuntosGrupo(new Punto_grupo(2,2,1,29));
            consultasDao.insertPuntosGrupo(new Punto_grupo(3,5,1,80));
            consultasDao.insertPuntosGrupo(new Punto_grupo(4,7,1,25));

            consultasDao.insertPuntosGrupo(new Punto_grupo(5,2,2,12));
            consultasDao.insertPuntosGrupo(new Punto_grupo(6,5,2,80));
            consultasDao.insertPuntosGrupo(new Punto_grupo(7,6,2,10));
            consultasDao.insertPuntosGrupo(new Punto_grupo(8,7,2,25));
            consultasDao.insertPuntosGrupo(new Punto_grupo(9,8,2,10));

            consultasDao.insertPuntosGrupo(new Punto_grupo(10,2,3,26));
            consultasDao.insertPuntosGrupo(new Punto_grupo(11,3,3,35));
            consultasDao.insertPuntosGrupo(new Punto_grupo(12,5,3,50));
            consultasDao.insertPuntosGrupo(new Punto_grupo(13,7,3,25));



            return null;
        }
    }


}
