package com.inblexapp.nutrition_app.Database.Tables;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tipos_porciones")
public class Tipo_porcion {

    @PrimaryKey
    private double id;

    private int tipo;

    private String nombre;

    private String abreviatura;

    public Tipo_porcion(double id, int tipo, String nombre, String abreviatura) {
        this.id = id;
        this.tipo = tipo;
        this.nombre = nombre;
        this.abreviatura = abreviatura;
    }

    public double getId() {
        return id;
    }

    public int getTipo() {
        return tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getAbreviatura() {
        return abreviatura;
    }
}
