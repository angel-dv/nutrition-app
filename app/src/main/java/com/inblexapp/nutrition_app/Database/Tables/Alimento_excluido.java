package com.inblexapp.nutrition_app.Database.Tables;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "alimentos_excluidos",foreignKeys = @ForeignKey(entity = Alimento.class,parentColumns = "id",childColumns = "alimentos_id"))
public class Alimento_excluido {

    @PrimaryKey
    private int id;

    private int alimentos_id;

    public Alimento_excluido(int id, int alimentos_id) {
        this.id = id;
        this.alimentos_id = alimentos_id;
    }

    public int getId() {
        return id;
    }

    public int getAlimentos_id() {
        return alimentos_id;
    }
}
