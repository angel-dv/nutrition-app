package com.inblexapp.nutrition_app.Utils;

public class Constants {
    public static final String API_BASE_URL = "http://lugar1.zapto.org:8080/api/";

    public static final String LOGIN_USER_TOKEN = "LOGIN_USER_TOKEN";
    public static final String ID_FOOD = "ID_FOOD";
    public static final String ALL_CATEGORY_FOOD_POINTS = "ALL_CATEGORY_FOOD_POINTS";
    public static final String CATEGORY_FOOD_TITLE = "CATEGORY_FOOD_TITLE";
    public static final String CATEGORY_FOOD_COLOR = "CATEGORY_FOOD_COLOR";
    public static final String CATEGORY_FOOD_ID = "CATEGORY_FOOD_ID";
    public static final String CATEGORY_GROUP_ID = "CATEGORY_GROUP_ID";

    public static final String ARG_LAYOUT_RES_ID = "ARG_LAYOUT_RES_ID";
    public static final String MY_INTRO = "MY_INTRO";

    // INFORMACION DEL USUARIO
    public static final String INFO_USER_GENDER = "INFO_USER_GENDER";
    public static final String INFO_USER_NAME = "INFO_USER_NAME";
    public static final String INFO_USER_OLD = "INFO_USER_OLD";
    public static final String INFO_USER_HEIGHT = "INFO_USER_HEIGHT";
    public static final String INFO_USER_WEIGHT = "INFO_USER_WEIGHT";
    public static final String INFO_USER_DATA = "INFO_USER_DATA";

    // ID DE LA BUSQUEDA DE CADA INGREDIENTE
    public static final String INGREDIENT_ONE_ID = "INGREDIENT_ONE_ID";


    //Objetives
    public static final String[] OBJETIVE_FOOD1 = new String[] {"C1GROUP1","C1GROUP2","C1GROUP3","C1GROUP4","C1GROUP5","C1GROUP6","C1GROUP7","C1GROUP8"};
    public static final String[] OBJETIVE_FOOD2 = new String[] {"C2GROUP1","C2GROUP2","C2GROUP3","C2GROUP4","C2GROUP5","C2GROUP6","C2GROUP7","C2GROUP8"};
    public static final String[] OBJETIVE_FOOD3 = new String[] {"C3GROUP1","C3GROUP2","C3GROUP3","C3GROUP4","C3GROUP5","C3GROUP6","C3GROUP7","C3GROUP8"};
    public static final String[] OBJETIVE_FOOD4 = new String[] {"C4GROUP1","C4GROUP2","C4GROUP3","C4GROUP4","C4GROUP5","C4GROUP6","C4GROUP7","C4GROUP8"};
    public static final String[] OBJETIVE_FOOD5 = new String[] {"C5GROUP1","C5GROUP2","C5GROUP3","C5GROUP4","C5GROUP5","C5GROUP6","C5GROUP7","C5GROUP8"};

    public static final String[] OBJETIVE_FOOD1_DONE = new String[] {"C1GROUP1D","C1GROUP2D","C1GROUP3D","C1GROUP4D","C1GROUP5D","C1GROUP6D","C1GROUP7D","C1GROUP8D"};
    public static final String[] OBJETIVE_FOOD2_DONE = new String[] {"C2GROUP1D","C2GROUP2D","C2GROUP3D","C2GROUP4D","C2GROUP5D","C2GROUP6D","C2GROUP7D","C2GROUP8D"};
    public static final String[] OBJETIVE_FOOD3_DONE = new String[] {"C3GROUP1D","C3GROUP2D","C3GROUP3D","C3GROUP4D","C3GROUP5D","C3GROUP6D","C3GROUP7D","C3GROUP8D"};
    public static final String[] OBJETIVE_FOOD4_DONE = new String[] {"C4GROUP1D","C4GROUP2D","C4GROUP3D","C4GROUP4D","C4GROUP5D","C4GROUP6D","C4GROUP7D","C4GROUP8D"};
    public static final String[] OBJETIVE_FOOD5_DONE = new String[] {"C5GROUP1D","C5GROUP2D","C5GROUP3D","C5GROUP4D","C5GROUP5D","C5GROUP6D","C5GROUP7D","C5GROUP8D"};

    public static final String FORMAT_TIME = "FORMAT_TIME";
}
