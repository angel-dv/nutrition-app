package com.inblexapp.nutrition_app.Utils.Intro;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import com.github.appintro.AppIntro;
import com.inblexapp.nutrition_app.Activities.DataUserActivity;
import com.inblexapp.nutrition_app.R;
import com.inblexapp.nutrition_app.Utils.Constants;
import com.inblexapp.nutrition_app.Utils.SharedPreferencesManager;

public class MyIntro extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        conf();
    }

    private void conf() {
        addSlide(SliderMainFragment.newInstance(R.layout.activity_welcome));
        addSlide(SliderMainFragment.newInstance(R.layout.activity_data_user_example));
        addSlide(SliderMainFragment.newInstance(R.layout.activity_data_user_points));
        addSlide(SliderMainFragment.newInstance(R.layout.activity_data_user_objective));

        setSkipText("OMITIR");
        setDoneText("CONTINUAR");
        showStatusBar(false);
        setVibrate(true);
        isColorTransitionsEnabled();
        setIndicatorColor(getResources().getColor(android.R.color.black), getResources().getColor(android.R.color.darker_gray));
        setColorSkipButton(getResources().getColor(android.R.color.darker_gray));
        setColorDoneText(getResources().getColor(android.R.color.black));
        setNextArrowColor(getResources().getColor(android.R.color.black));
        setSeparatorColor(getResources().getColor(android.R.color.transparent));
    }

    @Override
    protected void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        Intent intent = new Intent(getApplicationContext(), DataUserActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        SharedPreferencesManager.setSomeBooleanValue(Constants.MY_INTRO, false);
        finish();
    }

    @Override
    protected void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent intent = new Intent(getApplicationContext(), DataUserActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        SharedPreferencesManager.setSomeBooleanValue(Constants.MY_INTRO, false);
        finish();
    }
}
