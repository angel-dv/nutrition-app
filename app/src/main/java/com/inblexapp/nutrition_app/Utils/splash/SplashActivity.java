package com.inblexapp.nutrition_app.Utils.splash;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.inblexapp.nutrition_app.Activities.DataUserActivity;
import com.inblexapp.nutrition_app.Activities.NutriMainActivity;
import com.inblexapp.nutrition_app.Utils.Constants;
import com.inblexapp.nutrition_app.Utils.Intro.MyIntro;
import com.inblexapp.nutrition_app.Utils.SharedPreferencesManager;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent i = new Intent(SplashActivity.this, NutriMainActivity.class);
        startActivity(i);
        finish();
    }
}
