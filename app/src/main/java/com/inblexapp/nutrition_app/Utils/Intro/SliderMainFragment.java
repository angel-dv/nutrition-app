package com.inblexapp.nutrition_app.Utils.Intro;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inblexapp.nutrition_app.R;
import com.inblexapp.nutrition_app.Utils.Constants;

public class SliderMainFragment extends Fragment {

    private int layoutResId;

    public SliderMainFragment() {
    }

    public static SliderMainFragment newInstance(int layoutResId) {
        SliderMainFragment sliderMainFragment = new SliderMainFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ARG_LAYOUT_RES_ID, layoutResId);
        sliderMainFragment.setArguments(args);
        return sliderMainFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null && getArguments().containsKey(Constants.ARG_LAYOUT_RES_ID))
            layoutResId = getArguments().getInt(Constants.ARG_LAYOUT_RES_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(layoutResId, container, false);
    }
}