package com.inblexapp.nutrition_app.Utils;

import android.app.Application;
import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RelativeTime extends Application {

    public static String timeFormatAMPM(long timestamp, boolean typeFormat) {

        String dateString = "";
        SimpleDateFormat formatter;

        if (typeFormat) {
            formatter = new SimpleDateFormat("HH:mm"); // 24 Horas
            dateString = formatter.format(new Date(timestamp));
            return  dateString;
        } else {
            formatter = new SimpleDateFormat("hh:mm a"); // 12 Horas
            dateString = formatter.format(new Date(timestamp));
            return  dateString;
        }
    }

}
