package com.inblexapp.nutrition_app.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.inblexapp.nutrition_app.Entities.Objectives.Objective;
import com.inblexapp.nutrition_app.R;

import java.util.List;

public class ObjectivesAdapter extends RecyclerView.Adapter<ObjectivesAdapter.ViewHolder> {

    private List<Objective> objectives;
    private Context context;

    public ObjectivesAdapter(List<Objective> objectives, Context context) {
        this.objectives = objectives;
        this.context = context;
    }

    @NonNull
    @Override
    public ObjectivesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_objective, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ObjectivesAdapter.ViewHolder holder, int position) {
        holder.objective = objectives.get(position);

        holder.cbObjectiveDone.setText(holder.objective.getName());
        String points = holder.objective.getPointsDone() + "/" + holder.objective.getObjectivePoints();
        holder.tvObjectivesPoints.setText(points);
        holder.cbObjectiveDone.setChecked(holder.objective.isDone());

    }

    @Override
    public int getItemCount() {
        if (objectives.isEmpty()) return 0;
        else return objectives.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View v;
        public Objective objective;
        public final CheckBox cbObjectiveDone;
        public final TextView tvObjectivesPoints;

        public ViewHolder(View itemView) {
            super(itemView);
            v = itemView;
            cbObjectiveDone = v.findViewById(R.id.cbObjectiveDone);
            tvObjectivesPoints = v.findViewById(R.id.tvObjectivePoints);
        }
    }

}
