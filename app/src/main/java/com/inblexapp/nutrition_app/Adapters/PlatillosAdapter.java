package com.inblexapp.nutrition_app.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.inblexapp.nutrition_app.Database.Tables.Platillo;
import com.inblexapp.nutrition_app.R;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PlatillosAdapter extends BaseAdapter {

    private Context context;
    private List<Platillo> platillos = new ArrayList<>();

    public PlatillosAdapter(Context context){
        this.context = context;
    }


    @Override
    public int getCount() {
        return platillos.size();
    }

    @Override
    public Object getItem(int i) {
        return platillos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Platillo platillo = (Platillo) getItem(i);
        view = LayoutInflater.from(context).inflate(R.layout.item_suggested_dishes,null);

        CircleImageView imgPlatillo = view.findViewById(R.id.circleImageViewDish);
        TextView txtNombrePlatillo = view.findViewById(R.id.textViewNameDish);
        TextView txtPuntosPlatillo = view.findViewById(R.id.textViewPointDish);
        TextView txtNPuntosPlatillo = view.findViewById(R.id.textViewNamePointDish);
        Button btnVerMas = view.findViewById(R.id.buttonSeeMore);

        txtNombrePlatillo.setText(platillo.getNombre());
        txtPuntosPlatillo.setText(platillo.getPuntos());

        return view;
    }

    public void setPlatillos(List<Platillo> platillos){
        this.platillos = platillos;
        notifyDataSetChanged();
    }
}
