package com.inblexapp.nutrition_app.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.inblexapp.nutrition_app.Activities.CategoryFoodRequirementsActivity;
import com.inblexapp.nutrition_app.Database.Querys.IngredienteCompuesto;
import com.inblexapp.nutrition_app.Entities.ExampleIngredients.Ingredients;
import com.inblexapp.nutrition_app.R;

import java.util.List;

public class IngredientsAdapter extends RecyclerView.Adapter<IngredientsAdapter.ViewHolder> {

    private OnItemClickListener listener;
    private List<IngredienteCompuesto> ingredientsList;
    private Context ctx;

    public IngredientsAdapter(Context ctx, List<IngredienteCompuesto> ingredientsList) {
        this.ingredientsList = ingredientsList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ingredients, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        IngredienteCompuesto ingredient = ingredientsList.get(position);
        holder.tvName.setText(ingredient.getNombre());
        String p = ingredient.getPuntos() + " puntos";
        holder.tvPoint.setText(p);
        holder.tvMeasure.setText(ingredient.getPorcion());
        holder.tvContent.setText(ingredient.getCantidad());
    }

    @Override
    public int getItemCount() {
        if (ingredientsList == null)
            return 0;
        else return ingredientsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View v;
        TextView tvName, tvPoint, tvMeasure, tvContent;

        public ViewHolder(View view) {
            super(view);
            v = view;
            tvName = v.findViewById(R.id.textViewItemIngredientName);
            tvPoint = v.findViewById(R.id.textViewItemIngredientPoint);
            tvMeasure = v.findViewById(R.id.textViewItemIngredientMeasure);
            tvContent = v.findViewById(R.id.textViewItemIngredientContent);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClicked(ingredientsList.get(position));
                    }
                }
            });
        }

    }
    public interface OnItemClickListener {
        void onItemClicked(IngredienteCompuesto ingredients);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

}
