package com.inblexapp.nutrition_app.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.inblexapp.nutrition_app.Activities.SeeFoodsActivity;
import com.inblexapp.nutrition_app.Database.Tables.Platillo;
import com.inblexapp.nutrition_app.Entities.ExampleCategoryPointsFood.CategoriesFoodPoints;
import com.inblexapp.nutrition_app.Entities.ExampleSuggetions.SuggestedDishes;
import com.inblexapp.nutrition_app.Entities.Objectives.DishesListener;
import com.inblexapp.nutrition_app.Entities.Objectives.DishesListener2;
import com.inblexapp.nutrition_app.Entities.Objectives.PlatilloSel;
import com.inblexapp.nutrition_app.R;
import com.inblexapp.nutrition_app.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

import de.hdodenhof.circleimageview.CircleImageView;

public class SuggetionAdapterDos extends RecyclerView.Adapter<SuggetionAdapterDos.ViewHolder>{

    private OnItemClickListener listener;
    private List<PlatilloSel> suggestedDishesList;
    private Context ctx;
    DishesListener dishesListener;

    public SuggetionAdapterDos(Context context, List<PlatilloSel> suggestedDishesList, DishesListener dishesListener) {
        this.ctx = context;
        this.suggestedDishesList = suggestedDishesList;
        this.dishesListener = dishesListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_suggested_dishes, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.suggestedDishes = suggestedDishesList.get(position);

        //holder.clDish.setCircleBackgroundResource(R.drawable.tacos_lechuga);
        Glide.with(ctx)
                .load(holder.suggestedDishes.getRuta_imagen())
                .placeholder(R.drawable.tacos_lechuga)
                .into(holder.clDish);
        holder.tvName.setText(holder.suggestedDishes.getNombre());
        holder.tvPoint.setText(String.valueOf(holder.suggestedDishes.getPuntos()));
        holder.tvNamePoint.setText("Puntos");
        holder.llItemDish.setBackgroundResource(R.drawable.button_background_gray);
        holder.btnSeeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, SeeFoodsActivity.class);
                intent.putExtra(Constants.ID_FOOD, suggestedDishesList.get(position).getId());
                ArrayList<CategoriesFoodPoints> categoriesFoodPoints = new ArrayList<>();
//                categoriesFoodPoints.addAll(holder.suggestedDishes.getCategoriesFoodPoints());
                intent.putExtra(Constants.ALL_CATEGORY_FOOD_POINTS, categoriesFoodPoints);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (suggestedDishesList == null)
            return 0;
        else return suggestedDishesList.size();
    }

    public List<PlatilloSel> getSelectedDishes() {
        List<PlatilloSel> platilloSels = new ArrayList<>();
        for (PlatilloSel platilloSel : suggestedDishesList) {
            if (platilloSel.isSelected()) {
                platilloSels.add(platilloSel);
            }
        }
        return platilloSels;
    }

    public void actDatos(List<PlatilloSel> platilloSels){
        suggestedDishesList.clear();
        suggestedDishesList.addAll(platilloSels);
        notifyDataSetChanged();
    }

    public void deSelected(){
        for (PlatilloSel p: suggestedDishesList) {
            p.setSelected(false);
        }
        notifyDataSetChanged();
    }



    public class ViewHolder extends RecyclerView.ViewHolder{
        public final View v;
        public PlatilloSel suggestedDishes;
        public final CircleImageView clDish;
        public final TextView tvName, tvPoint, tvNamePoint;
        public final Button btnSeeMore;
        public final CoordinatorLayout clSuggestedDish;
        public final LinearLayout llItemDish;

        public ViewHolder(View view) {
            super(view);
            v = view;
            clDish = v.findViewById(R.id.circleImageViewDish);
            tvName = v.findViewById(R.id.textViewNameDish);
            tvPoint = v.findViewById(R.id.textViewPointDish);
            tvNamePoint = v.findViewById(R.id.textViewNamePointDish);
            btnSeeMore = v.findViewById(R.id.buttonSeeMore);
            clSuggestedDish = v.findViewById(R.id.clSuggestedDish);
            llItemDish = v.findViewById(R.id.llItemDish);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClicked(suggestedDishesList.get(position));
                    }
                }
            });

            clSuggestedDish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (suggestedDishes.isSelected()) {
                        llItemDish.setBackgroundResource(R.drawable.button_background_gray);
                        suggestedDishes.setSelected(false);
                        if (getSelectedDishes().isEmpty()) {
                            dishesListener.onDishAction(false);
                        }
                    } else {
                        llItemDish.setBackgroundResource(R.drawable.button_background_gray_selected);
                        suggestedDishes.setSelected(true);
                        dishesListener.onDishAction(true);
                    }
                }
            });


        }
    }

    public interface OnItemClickListener {
        void onItemClicked(PlatilloSel suggestedDishes);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

}