package com.inblexapp.nutrition_app.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.inblexapp.nutrition_app.Entities.Results.Objetivo;
import com.inblexapp.nutrition_app.R;

import java.util.List;

public class ObjetivosDIariosAdapter extends BaseAdapter {

    private List<Objetivo> objetivos;
    private Context context;

    public ObjetivosDIariosAdapter(List<Objetivo> objetivos, Context context) {
        this.objetivos = objetivos;
        this.context = context;
    }

    @Override
    public int getCount() {
        return objetivos.size();
    }

    @Override
    public Object getItem(int i) {
        return objetivos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Objetivo obj = (Objetivo) getItem(i);

        view = LayoutInflater.from(context).inflate(R.layout.item_lista_objetivos,null);
        ImageView done = view.findViewById(R.id.imgObj);
        TextView nombre = view.findViewById(R.id.txtObjNombre);
        TextView puntos = view.findViewById(R.id.txtObjPuntosObtenidos);
        TextView objetivo = view.findViewById(R.id.txtObjPuntosBase);

        done.setImageResource(obj.getImage());
        nombre.setText(obj.getNombre());
        puntos.setText(String.valueOf(obj.getPuntos()));
        String obn = " de " + obj.getObjetivo();
        objetivo.setText(obn);

        puntos.setTextColor(colorPuntaje(obj.getPuntos(),obj.getObjetivo()));
        return view;
    }

    private int colorPuntaje(int puntaje, int objetivo){
        int color = 0;
        if(puntaje == objetivo) color = -8339648;
        else {
            int dif = 0;
            if(puntaje < objetivo){
                dif = objetivo - puntaje;
            } else {
                dif = puntaje - objetivo;
            }
            if(dif > 9) color = -2937068;
            else color = -26109;

        }
        return color;
    }
}
