package com.inblexapp.nutrition_app.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.inblexapp.nutrition_app.Activities.IngredientOneActivity;
import com.inblexapp.nutrition_app.Activities.SeeFoodsActivity;
import com.inblexapp.nutrition_app.Entities.IngredientsAll.IngredientsAll;
import com.inblexapp.nutrition_app.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class IngredientsAllAdapter extends RecyclerView.Adapter<IngredientsAllAdapter.ViewHolder> {

    private OnItemClickListener listener;
    private List<IngredientsAll> ingredientsAllList;
    private Context ctx;

    public IngredientsAllAdapter(Context context, List<IngredientsAll> ingredientsAllList) {
        this.ctx = context;
        this.ingredientsAllList = ingredientsAllList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_ingredients, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        IngredientsAll ingredientsAll = ingredientsAllList.get(position);

//        holder.clDish.setCircleBackgroundResource(R.drawable.tacos_lechuga);
        Glide.with(ctx)
                .load(ingredientsAll.getImage())
                .placeholder(R.drawable.tacos_lechuga)
                .into(holder.clIngredients);
        holder.tvName.setText(ingredientsAll.getName());
        holder.tvPoint.setText(String.valueOf(ingredientsAll.getPoint()));
        holder.tvNamePoint.setText("Puntos");

        holder.btnSeeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, IngredientOneActivity.class);
                intent.putExtra("INGREDIENT_ONE_ID", ingredientsAllList.get(position).getId());
                intent.putExtra("INGREDIENT_ONE_NAME", ingredientsAllList.get(position).getName());
                intent.putExtra("INGREDIENT_ONE_POINT", ingredientsAllList.get(position).getPoint());
                intent.putExtra("INGREDIENT_ONE_PROPERTIES", ingredientsAllList.get(position).getProperties());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (ingredientsAllList == null)
            return 0;
        else return ingredientsAllList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View v;
        public IngredientsAll ingredientsAll;
        public final CircleImageView clIngredients;
        public final TextView tvName, tvPoint, tvNamePoint;
        public final Button btnSeeMore;

        public ViewHolder(View view) {
            super(view);
            v = view;
            clIngredients = v.findViewById(R.id.circleImageViewIngredient);
            tvName = v.findViewById(R.id.textViewNameIngredient);
            tvPoint = v.findViewById(R.id.textViewPointIngredient);
            tvNamePoint = v.findViewById(R.id.textViewNamePointIngredient);
            btnSeeMore = v.findViewById(R.id.buttonSeeMoreIngredient);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClicked(ingredientsAllList.get(position));
                    }
                }
            });
        }

    }
    public interface OnItemClickListener {
        void onItemClicked(IngredientsAll ingredientsAll);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

}
