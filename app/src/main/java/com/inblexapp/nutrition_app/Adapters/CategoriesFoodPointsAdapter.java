package com.inblexapp.nutrition_app.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.inblexapp.nutrition_app.Activities.CategoryFoodRequirementsActivity;
import com.inblexapp.nutrition_app.Activities.SeeFoodsActivity;
import com.inblexapp.nutrition_app.Entities.ExampleCategoryPointsFood.CategoriesFoodPoints;
import com.inblexapp.nutrition_app.R;
import com.inblexapp.nutrition_app.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class CategoriesFoodPointsAdapter extends RecyclerView.Adapter<CategoriesFoodPointsAdapter.ViewHolder> {

    private OnItemClickListener listener;
    private List<CategoriesFoodPoints> categoriesFoodPointsList;
    private Context ctx;
    private double idPlatillo;

    public void setIdPlatillo(double idPlatillo) {
        this.idPlatillo = idPlatillo;
    }

    public CategoriesFoodPointsAdapter(Context ctx, List<CategoriesFoodPoints> categoriesFoodPointsList) {
        this.categoriesFoodPointsList = categoriesFoodPointsList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_categories_food_points, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CategoriesFoodPoints categoriesFoodPoints = categoriesFoodPointsList.get(position);

        holder.tvTitle.setText(categoriesFoodPoints.getTitle());
        holder.btnColor.setText(String.format("%s puntos", categoriesFoodPoints.getPoints()));

        int color = categoriesFoodPoints.getColor();

        switch (color) {
            case 1:
                holder.btnColor.getBackground().setTint(ctx.getResources().getColor(R.color.colorPink));
                break;
            case 2:
                holder.btnColor.getBackground().setTint(ctx.getResources().getColor(R.color.colorLightGreen));
                break;
            case 3:
                holder.btnColor.getBackground().setTint(ctx.getResources().getColor(R.color.colorYellow));
                break;
            case 4:
                holder.btnColor.getBackground().setTint(ctx.getResources().getColor(R.color.colorOrange));
                break;
            case 5:
                holder.btnColor.getBackground().setTint(ctx.getResources().getColor(R.color.colorRed));
                break;
            case 6:
                holder.btnColor.getBackground().setTint(ctx.getResources().getColor(R.color.colorLightBlue));
                break;
            case 7:
                holder.btnColor.getBackground().setTint(ctx.getResources().getColor(R.color.colorLightPurple));
                break;
            case 8:
                holder.btnColor.getBackground().setTint(ctx.getResources().getColor(R.color.colorDarkGreen));
                break;
            default:
                holder.btnColor.getBackground().setTint(ctx.getResources().getColor(R.color.colorGray));
                break;
        }

        holder.btnColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, CategoryFoodRequirementsActivity.class);
                intent.putExtra(Constants.CATEGORY_FOOD_TITLE, categoriesFoodPoints.getTitle());
                intent.putExtra(Constants.CATEGORY_FOOD_COLOR, categoriesFoodPoints.getColor());
                intent.putExtra(Constants.CATEGORY_GROUP_ID,categoriesFoodPoints.getId());
                intent.putExtra(Constants.CATEGORY_FOOD_ID,idPlatillo);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                ArrayList<CategoriesFoodPoints> categoriesFoodPoints = new ArrayList<>();
//                categoriesFoodPoints.addAll(holder.suggestedDishes.getCategoriesFoodPoints());
//                intent.putExtra("CategoriesFoodPoints", categoriesFoodPoints);
                ctx.startActivity(intent);
            }
        });

//     int color = ctx.getResources().getColor(R.color.colorPink);
//     holder.btnColor.setBackgroundResource(categoriesFoodPoints.getColor());

    }

    @Override
    public int getItemCount() {
        if (categoriesFoodPointsList == null)
            return 0;
        else return categoriesFoodPointsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        Button btnColor;

        public ViewHolder(@NonNull View v) {
            super(v);
            tvTitle = v.findViewById(R.id.textViewCategoryFoodPointTitle);
            btnColor = v.findViewById(R.id.buttonCategoryFoodPoints);

            v.setOnClickListener(v1 -> {
                int position = getAdapterPosition();
                if (listener != null && position != RecyclerView.NO_POSITION) {
                    listener.onItemClicked(categoriesFoodPointsList.get(position));
                }
            });
        }

    }

    public interface OnItemClickListener {
        void onItemClicked(CategoriesFoodPoints categoriesFoodPoints);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
