package com.inblexapp.nutrition_app.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.florent37.shapeofview.shapes.CircleView;
import com.inblexapp.nutrition_app.Activities.IngredientOneActivity;
import com.inblexapp.nutrition_app.Database.Tables.Alimento;
import com.inblexapp.nutrition_app.R;
import com.inblexapp.nutrition_app.Utils.Constants;

import java.util.List;

public class FoodsAdapter extends RecyclerView.Adapter<FoodsAdapter.ViewHolder> {

    private OnItemClickListener onItemClickListener;
    private List<Alimento> foodsList;
    private Context ctx;
    private int id = 0;

    public FoodsAdapter(Context ctx, List<Alimento> foodsList) {
        this.foodsList = foodsList;
        this.ctx = ctx;
    }

    public void setFoods(List<Alimento> foodsList){
        this.foodsList = foodsList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista_alimentos, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Alimento alimento = foodsList.get(position);

        holder.tvFoodName.setText(alimento.getNombre());
        holder.btnFoodViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id = alimento.getId();
                Intent i = new Intent(ctx, IngredientOneActivity.class);
                i.putExtra(Constants.INGREDIENT_ONE_ID, id);
                ctx.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (foodsList == null)
            return 0;
        else return foodsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleView imgFood;
        TextView tvFoodName;
        Button btnFoodViewMore;

        public ViewHolder(@NonNull View v) {
            super(v);
            imgFood = v.findViewById(R.id.imgAlim);
            tvFoodName = v.findViewById(R.id.txtAlimNombre);
            btnFoodViewMore = v.findViewById(R.id.btnAlimVerMas);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (onItemClickListener != null && position != RecyclerView.NO_POSITION) {
                        onItemClickListener.onItemClick(foodsList.get(position));
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Alimento alimento);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

}
