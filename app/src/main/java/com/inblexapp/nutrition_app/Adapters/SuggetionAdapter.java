package com.inblexapp.nutrition_app.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.inblexapp.nutrition_app.Activities.SeeFoodsActivity;
import com.inblexapp.nutrition_app.Entities.ExampleCategoryPointsFood.CategoriesFoodPoints;
import com.inblexapp.nutrition_app.Entities.ExampleSuggetions.SuggestedDishes;
import com.inblexapp.nutrition_app.R;
import com.inblexapp.nutrition_app.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SuggetionAdapter extends RecyclerView.Adapter<SuggetionAdapter.ViewHolder> {

    private OnItemClickListener listener;
    private List<SuggestedDishes> suggestedDishesList;
    private Context ctx;

    public SuggetionAdapter(Context context, List<SuggestedDishes> suggestedDishesList) {
        this.ctx = context;
        this.suggestedDishesList = suggestedDishesList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_suggested_dishes, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.suggestedDishes = suggestedDishesList.get(position);

//        holder.clDish.setCircleBackgroundResource(R.drawable.tacos_lechuga);
        Glide.with(ctx)
                .load(holder.suggestedDishes.getImage())
                .placeholder(R.drawable.tacos_lechuga)
                .into(holder.clDish);
        holder.tvName.setText(holder.suggestedDishes.getName());
        holder.tvPoint.setText(String.valueOf(holder.suggestedDishes.getPoint()));
        holder.tvNamePoint.setText("Puntos");
        holder.btnSeeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, SeeFoodsActivity.class);
                intent.putExtra(Constants.ID_FOOD, suggestedDishesList.get(position));
                ArrayList<CategoriesFoodPoints> categoriesFoodPoints = new ArrayList<>();
                categoriesFoodPoints.addAll(holder.suggestedDishes.getCategoriesFoodPoints());
                intent.putExtra(Constants.ALL_CATEGORY_FOOD_POINTS, categoriesFoodPoints);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (suggestedDishesList == null)
            return 0;
        else return suggestedDishesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View v;
        public SuggestedDishes suggestedDishes;
        public final CircleImageView clDish;
        public final TextView tvName, tvPoint, tvNamePoint;
        public final Button btnSeeMore;

        public ViewHolder(View view) {
            super(view);
            v = view;
            clDish = v.findViewById(R.id.circleImageViewDish);
            tvName = v.findViewById(R.id.textViewNameDish);
            tvPoint = v.findViewById(R.id.textViewPointDish);
            tvNamePoint = v.findViewById(R.id.textViewNamePointDish);
            btnSeeMore = v.findViewById(R.id.buttonSeeMore);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClicked(suggestedDishesList.get(position));
                    }
                }
            });
        }

    }
    public interface OnItemClickListener {
        void onItemClicked(SuggestedDishes suggestedDishes);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

}
