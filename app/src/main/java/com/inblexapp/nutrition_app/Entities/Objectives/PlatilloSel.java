package com.inblexapp.nutrition_app.Entities.Objectives;

public class PlatilloSel {
    private double id;
    private String nombre;
    private int puntos;
    private String ruta_imagen;
    private boolean selected;

    public PlatilloSel(double id, String nombre, int puntos, String ruta_imagen) {
        this.id = id;
        this.nombre = nombre;
        this.puntos = puntos;
        this.ruta_imagen = ruta_imagen;
        selected = false;
    }

    public double getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public int getPuntos() {
        return puntos;
    }

    public String getRuta_imagen() {
        return ruta_imagen;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
