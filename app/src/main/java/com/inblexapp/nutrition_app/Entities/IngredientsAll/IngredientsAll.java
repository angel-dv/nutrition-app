package com.inblexapp.nutrition_app.Entities.IngredientsAll;

public class IngredientsAll {

    private int id;
    private int image;
    private String name;
    private String properties;
    private String steps;
    private int point;

    public IngredientsAll(int id, int image, String name, String properties, String steps, int point) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.properties = properties;
        this.steps = steps;
        this.point = point;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }
}