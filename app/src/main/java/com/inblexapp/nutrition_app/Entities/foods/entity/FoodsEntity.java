package com.inblexapp.nutrition_app.Entities.foods.entity;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.inblexapp.nutrition_app.Entities.foods.GrupoAlimento;

//@Entity(tableName = "alimentos")
public class FoodsEntity {

//    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("descripcion")
    @Expose
    private String descripcion;
    @SerializedName("puntos")
    @Expose
    private int puntos;
//    @SerializedName("grupoAlimento")
//    @Expose
//    private GrupoAlimento grupoAlimento;

    public FoodsEntity() {
    }

    public FoodsEntity(String nombre, String descripcion, int puntos,
//                       GrupoAlimento grupoAlimento,
                       int id) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.puntos = puntos;
//        this.grupoAlimento = grupoAlimento;
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

//    public GrupoAlimento getGrupoAlimento() {
//        return grupoAlimento;
//    }
//
//    public void setGrupoAlimento(GrupoAlimento grupoAlimento) {
//        this.grupoAlimento = grupoAlimento;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
