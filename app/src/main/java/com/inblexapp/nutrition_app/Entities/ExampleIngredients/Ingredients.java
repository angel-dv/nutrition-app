package com.inblexapp.nutrition_app.Entities.ExampleIngredients;

public class Ingredients {

    private int id;
    private String name;
    private String points;
    private String measure;
    private String content;

    public Ingredients(int id, String name, String points, String measure, String content) {
        this.id = id;
        this.name = name;
        this.points = points;
        this.measure = measure;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
