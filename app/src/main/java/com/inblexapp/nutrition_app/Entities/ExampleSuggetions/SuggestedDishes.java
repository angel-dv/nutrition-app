package com.inblexapp.nutrition_app.Entities.ExampleSuggetions;

import android.os.Parcel;
import android.os.Parcelable;

import com.inblexapp.nutrition_app.Entities.ExampleCategoryPointsFood.CategoriesFoodPoints;

import java.util.ArrayList;
import java.util.List;

public class SuggestedDishes implements Parcelable {

    private int id;
    private int image;
    private String name;
    private String ingredients;
    private String steps;
    private int point;
    private List<CategoriesFoodPoints> categoriesFoodPoints = new ArrayList<>();

    public SuggestedDishes(int id, int image, String name, String ingredients, String steps, int point, List<CategoriesFoodPoints> categoriesFoodPoints) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.ingredients = ingredients;
        this.steps = steps;
        this.point = point;
        this.categoriesFoodPoints = categoriesFoodPoints;
    }

    protected SuggestedDishes(Parcel in) {
        id = in.readInt();
        image = in.readInt();
        name = in.readString();
        ingredients = in.readString();
        steps = in.readString();
        point = in.readInt();
        in.readList(categoriesFoodPoints, CategoriesFoodPoints.class.getClassLoader());
    }

    public static final Creator<SuggestedDishes> CREATOR = new Creator<SuggestedDishes>() {
        @Override
        public SuggestedDishes createFromParcel(Parcel in) {
            return new SuggestedDishes(in);
        }

        @Override
        public SuggestedDishes[] newArray(int size) {
            return new SuggestedDishes[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public List<CategoriesFoodPoints> getCategoriesFoodPoints() {
        return categoriesFoodPoints;
    }

    public void setCategoriesFoodPoints(List<CategoriesFoodPoints> categoriesFoodPoints) {
        this.categoriesFoodPoints = categoriesFoodPoints;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(image);
        dest.writeString(name);
        dest.writeString(ingredients);
        dest.writeString(steps);
        dest.writeInt(point);
        dest.writeList(categoriesFoodPoints);
    }
}