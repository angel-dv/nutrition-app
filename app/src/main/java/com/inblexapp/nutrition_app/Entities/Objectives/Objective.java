package com.inblexapp.nutrition_app.Entities.Objectives;

public class Objective {
    private String name;
    private int objectivePoints;
    private int pointsDone;
    private boolean done;

    public Objective(String name, int objectivePoints, int pointsDone) {
        this.name = name;
        this.objectivePoints = objectivePoints;
        this.pointsDone = pointsDone;
        if(pointsDone < objectivePoints) done = false;
        else done = true;
    }

    public String getName() {
        return name;
    }

    public int getObjectivePoints() {
        return objectivePoints;
    }

    public int getPointsDone() {
        return pointsDone;
    }

    public boolean isDone() {
        return done;
    }
}
