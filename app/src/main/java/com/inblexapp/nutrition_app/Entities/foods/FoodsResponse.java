package com.inblexapp.nutrition_app.Entities.foods;

import com.inblexapp.nutrition_app.Entities.foods.entity.FoodsEntity;

import java.util.List;

public class FoodsResponse {

    private List<FoodsEntity> results;

    public List<FoodsEntity> getResults() {
        return results;
    }

    public void setResults(List<FoodsEntity> results) {
        this.results = results;
    }
}
