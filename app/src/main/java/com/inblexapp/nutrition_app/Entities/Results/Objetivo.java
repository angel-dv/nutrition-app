package com.inblexapp.nutrition_app.Entities.Results;

import com.inblexapp.nutrition_app.R;

public class Objetivo {

    int image;
    String nombre;
    int puntos, objetivo;

    public Objetivo(String nombre, int puntos, int objetivo) {
        this.nombre = nombre;
        this.puntos = puntos;
        this.objetivo = objetivo;
        if (puntos >= objetivo) image = R.drawable.done;
        else image = R.drawable.not_done;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public int getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(int objetivo) {
        this.objetivo = objetivo;
    }
}
