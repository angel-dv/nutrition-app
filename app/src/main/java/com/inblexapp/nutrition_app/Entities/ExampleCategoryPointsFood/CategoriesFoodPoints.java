package com.inblexapp.nutrition_app.Entities.ExampleCategoryPointsFood;

import android.os.Parcel;
import android.os.Parcelable;

public class CategoriesFoodPoints implements Parcelable {

    private int id;
    private String title;
    private String points;
    private int color;

    public CategoriesFoodPoints(int id, String title, String points, int color) {
        this.id = id;
        this.title = title;
        this.points = points;
        this.color = color;
    }

    protected CategoriesFoodPoints(Parcel in) {
        id = in.readInt();
        title = in.readString();
        points = in.readString();
        color = in.readInt();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(points);
        dest.writeInt(color);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CategoriesFoodPoints> CREATOR = new Creator<CategoriesFoodPoints>() {
        @Override
        public CategoriesFoodPoints createFromParcel(Parcel in) {
            return new CategoriesFoodPoints(in);
        }

        @Override
        public CategoriesFoodPoints[] newArray(int size) {
            return new CategoriesFoodPoints[size];
        }
    };
}
