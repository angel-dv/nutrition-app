package com.inblexapp.nutrition_app.Entities.Objectives;

public interface DishesListener {
    void onDishAction(boolean isSelected);
}
