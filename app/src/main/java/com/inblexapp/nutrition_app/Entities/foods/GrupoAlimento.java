package com.inblexapp.nutrition_app.Entities.foods;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GrupoAlimento {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("nombre")
    @Expose
    private String nombre;

    public GrupoAlimento() {
    }

    public GrupoAlimento(String nombre, int id) {
        this.nombre = nombre;
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
