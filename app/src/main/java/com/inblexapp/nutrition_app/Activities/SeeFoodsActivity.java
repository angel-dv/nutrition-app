package com.inblexapp.nutrition_app.Activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.inblexapp.nutrition_app.Adapters.CategoriesFoodPointsAdapter;
import com.inblexapp.nutrition_app.Database.AlimentosViewModel;
import com.inblexapp.nutrition_app.Database.Querys.IngredienteCompuesto;
import com.inblexapp.nutrition_app.Database.Tables.Platillo;
import com.inblexapp.nutrition_app.Entities.ExampleCategoryPointsFood.CategoriesFoodPoints;
import com.inblexapp.nutrition_app.R;
import com.inblexapp.nutrition_app.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class SeeFoodsActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView tvIdFood, tvNameFood, tvPointFood, tvIngredients, tvIngredientsPoints, tvSteps;
    ImageView ivFood;
    Platillo suggestedDishes;
    double platillo_id;
    List<CategoriesFoodPoints> categoriesFoodPoints;
    RecyclerView rvCategoryFoodPoints;
    CategoriesFoodPointsAdapter adapter;
    List<IngredienteCompuesto> ingredientesCompuestos;
    String ingredientes;
    String puntos;

    private AlimentosViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_foods);
        viewModel = ViewModelProviders.of(this).get(AlimentosViewModel.class);

        Bundle bundle = getIntent().getExtras();
        platillo_id = bundle.getDouble(Constants.ID_FOOD);
        //categoriesFoodPoints = bundle.getParcelableArrayList(Constants.ALL_CATEGORY_FOOD_POINTS);

        toolbar = findViewById(R.id.toolbar);
        ivFood = findViewById(R.id.imageViewFood);
        tvNameFood = findViewById(R.id.textViewNameFood);
        tvPointFood = findViewById(R.id.textViewPointFood);
        tvIngredients = findViewById(R.id.textViewIngredients);
        tvIngredientsPoints = findViewById(R.id.textViewIngredientsPoints);
        tvSteps = findViewById(R.id.textViewSteps);
        tvIdFood = findViewById(R.id.textViewIdFood);
        rvCategoryFoodPoints = findViewById(R.id.recyclerViewCategoriesFoodPoints);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LinearLayoutManager linearLayoutManager = new GridLayoutManager(SeeFoodsActivity.this, 4);
        rvCategoryFoodPoints.setLayoutManager(linearLayoutManager);

        viewModel.setPlatilloById(platillo_id);
        viewModel.getPlatilloById().observe(this, new Observer<Platillo>() {
            @Override
            public void onChanged(Platillo platillo) {
                suggestedDishes = platillo;
                //int image = suggestedDishes.getRuta_Imagen();
                String name = suggestedDishes.getNombre();
                //String ingredients = suggestedDishes.getIngredientes();
                String steps = suggestedDishes.getInstrucciones();
                int point = suggestedDishes.getPuntos();

                /*Glide.with(SeeFoodsActivity.this)
                .load(image)
                .placeholder(R.drawable.tacos_lechuga)
                .into(ivFood);*/
                tvNameFood.setText(name);
                tvPointFood.setText(String.format("%s puntos", point));
                //tvIngredients.setText(ingredients);
                tvSteps.setText(steps);
            }
        });

        ingredientes = "";
        puntos = "";
        viewModel.setIngredientesPlatillo(platillo_id);
        viewModel.getIngredientesPlatillo().observe(this, new Observer<List<IngredienteCompuesto>>() {
            @Override
            public void onChanged(List<IngredienteCompuesto> ingredienteCompuestos) {
                ingredientesCompuestos = ingredienteCompuestos;
                for (IngredienteCompuesto ingrediente : ingredientesCompuestos) {
                    if (ingrediente.getPorcion().equals("Al gusto")) {
                        ingredientes = ingredientes +
                                ingrediente.getNombre() + " al gusto\n";
                    } else {
                        ingredientes = ingredientes +
                                ingrediente.getCantidad() + " " +
                                ingrediente.getPorcion() + " de " +
                                ingrediente.getNombre() + "\n";
                    }

                    puntos = puntos + ingrediente.getPuntos() + " puntos\n";
                }
                tvIngredients.setText(ingredientes);
                tvIngredientsPoints.setText(puntos);
            }
        });

        viewModel.setPuntosGruposPlatillo(platillo_id);
        viewModel.getPuntosGruposPlatillo().observe(this, new Observer<List<CategoriesFoodPoints>>() {
            @Override
            public void onChanged(List<CategoriesFoodPoints> categoriesFoodPointse) {
                categoriesFoodPoints = categoriesFoodPointse;
                adapter = new CategoriesFoodPointsAdapter(SeeFoodsActivity.this, categoriesFoodPoints);
                adapter.setIdPlatillo(platillo_id);
                rvCategoryFoodPoints.setHasFixedSize(true);
                rvCategoryFoodPoints.setAdapter(adapter);
            }
        });
//        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
//        rvCategoryFoodPoints.setLayoutManager(staggeredGridLayoutManager);
/*
        categoriesFoodPoints = new ArrayList<>();
        categoriesFoodPoints.add(new CategoriesFoodPoints(1, "Frutas", "15", 1));
        categoriesFoodPoints.add(new CategoriesFoodPoints(2, "Verduras", "24", 2));
        categoriesFoodPoints.add(new CategoriesFoodPoints(3, "Cereales", "124", 3));
        categoriesFoodPoints.add(new CategoriesFoodPoints(4, "Leguminosas", "94", 4));
        categoriesFoodPoints.add(new CategoriesFoodPoints(5, "Carnes", "5", 5));
        categoriesFoodPoints.add(new CategoriesFoodPoints(6, "Lacteos", "17", 6));
        categoriesFoodPoints.add(new CategoriesFoodPoints(7, "Grasas", "4", 7));
        categoriesFoodPoints.add(new CategoriesFoodPoints(8, "Libres", "37", 8));
*/


//        tvIdFood.setText(String.valueOf(platillo_id));

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }
}