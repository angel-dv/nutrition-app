package com.inblexapp.nutrition_app.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.inblexapp.nutrition_app.Adapters.SuggetionAdapter;
import com.inblexapp.nutrition_app.Adapters.SuggetionAdapterDos;
import com.inblexapp.nutrition_app.Data.viewmodel.NutritionViewModel;
import com.inblexapp.nutrition_app.Database.AlimentosViewModel;
import com.inblexapp.nutrition_app.Database.Querys.PlatilloPorGrupoYPuntos;
import com.inblexapp.nutrition_app.Database.Querys.PuntosGrupoPlatillo;
import com.inblexapp.nutrition_app.Database.Tables.Alimento;
import com.inblexapp.nutrition_app.Database.Tables.Grupo_alimento;
import com.inblexapp.nutrition_app.Database.Tables.Ingrediente;
import com.inblexapp.nutrition_app.Database.Tables.Platillo;
import com.inblexapp.nutrition_app.Database.Tables.Punto_grupo;
import com.inblexapp.nutrition_app.Database.Tables.Tipo_platillo;
import com.inblexapp.nutrition_app.Database.Tables.Tipo_porcion;
import com.inblexapp.nutrition_app.Entities.ExampleCategoryPointsFood.CategoriesFoodPoints;
import com.inblexapp.nutrition_app.Entities.ExampleSuggetions.SuggestedDishes;
import com.inblexapp.nutrition_app.Entities.Objectives.DishesListener;
import com.inblexapp.nutrition_app.Entities.Objectives.PlatilloSel;
import com.inblexapp.nutrition_app.R;
import com.inblexapp.nutrition_app.Utils.Constants;
import com.inblexapp.nutrition_app.Utils.Intro.MyIntro;
import com.inblexapp.nutrition_app.Utils.RelativeTime;
import com.inblexapp.nutrition_app.Utils.SharedPreferencesManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NutriMainActivity extends AppCompatActivity implements DishesListener {

    public static final String TAG = NutriMainActivity.class.getSimpleName();
    NutritionViewModel viewModel;

    Toolbar toolbar;
    RecyclerView rvSuggetions;
    TextView tvPoints, tvTime, tvName;
    Button btnSeeObjetive, btnAddDishes;
    ImageView ivOptions, imageViewExample;
    List<PlatilloSel> platilloSels;

    ArrayList<SuggestedDishes> suggestedDishesList;
    List<CategoriesFoodPoints> categoriesFoodPointsList;
    SuggetionAdapter suggetionAdapter;

    SuggetionAdapterDos adapter;

    private AlimentosViewModel viewModelDBL;

    int[] objectives_food1;
    int[] objectives_food1D;
    int[] dif_objectives1;

    private boolean mActive;
    private final Handler mHandler;

    private final Runnable mRunnable = new Runnable() {
        public void run() {
            int hour = 0;
            int min = 0;
            if (mActive) {
                if (tvTime != null) {
                    boolean isTypeFormatHour = SharedPreferencesManager.getSomeBooleanValue(Constants.FORMAT_TIME);

                    long time = new Date().getTime();

                    if (isTypeFormatHour) {
                        String relativeTime = RelativeTime.timeFormatAMPM(time, isTypeFormatHour);

                        String auxHour = relativeTime.substring(0, 2);
                        String auxMin = relativeTime.substring(3);
                        hour = Integer.parseInt(auxHour);
                        min = Integer.parseInt(auxMin);

                        if ((hour >= 7 && hour <= 10) && (min > 1 && min <= 30)) {
                            tvTime.setText("Desayuno");
                        } else if ((hour >= 11 && hour <= 13) && (min > 1 && min <= 20)) {
                            tvTime.setText("Colación 1");
                        } else if ((hour >= 14 && hour <= 17) && (min > 1 && min <= 30)) {
                            tvTime.setText("Comida");
                        } else if ((hour >= 18 && hour <= 20) && (min > 1 && min <= 20)) {
                            tvTime.setText("Colación 2");
                        } else if ((hour >= 21 && hour <= 22) && (min > 1 && min <= 30)) {
                            tvTime.setText("Cena");
                        } else {
                            tvTime.setText(relativeTime.toUpperCase());
                        }

                    } else {
                        String relativeTime = RelativeTime.timeFormatAMPM(time, isTypeFormatHour);

                        String auxHour = relativeTime.substring(0, 2);
                        String auxMin = relativeTime.substring(3, 5);
                        String auxSchedule = relativeTime.substring(6);
                        hour = Integer.parseInt(auxHour);
                        min = Integer.parseInt(auxMin);

                        Log.d(TAG, relativeTime);

                        if ((hour >= 7 && hour <= 10) && (auxSchedule.equals("a. m.")) && (min > 1 && min <= 30)) {
                            tvTime.setText("Desayuno");
                        } else if ((hour == 11) && (auxSchedule.equals("a. m.")) && (min > 1 && min <= 20)) {
                            tvTime.setText("Colación 1");
                        } else if ((hour == 12) && (auxSchedule.equals("p. m."))  && (min > 1 && min <= 20)) {
                            tvTime.setText("Colación 1");
                        } else if ((hour == 1) && (auxSchedule.equals("p. m."))  && (min > 1 && min <= 20)) {
                            tvTime.setText("Colación 1");
                        } else if ((hour >= 2 && hour <= 5) && (auxSchedule.equals("p. m.")) && (min > 1 && min <= 30)) {
                            tvTime.setText("Comida");
                        } else if ((hour >= 6 && hour <= 8) && (auxSchedule.equals("p. m.")) && (min > 1 && min <= 20)) {
                            tvTime.setText("Colación 2");
                        } else if ((hour >= 9 && hour <= 10) && (auxSchedule.equals("p. m.")) && (min > 1 && min <= 30)) {
                            tvTime.setText("Cena");
                        } else {
                            tvTime.setText(relativeTime.toUpperCase());
                        }
                    }

                }

                mHandler.postDelayed(mRunnable, 5000);
            }
        }
    };

    public NutriMainActivity() {
        mHandler = new Handler();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nutri_main);
//        viewModel = ViewModelProviders.of(this).get(NutritionViewModel.class);

        objectives_food1 = new int[8];
        objectives_food1D = new int[8];
        dif_objectives1 = new int[8];

        getAllObjetives();

        toolbar = findViewById(R.id.toolbar);
        tvName = findViewById(R.id.textViewMainName);
        tvPoints = findViewById(R.id.textViewPoints);
        tvTime = findViewById(R.id.textViewTime);
        btnSeeObjetive = findViewById(R.id.buttonSeeObjetive);
        ivOptions = findViewById(R.id.imageViewOptions);
        rvSuggetions = findViewById(R.id.recyclerViewSuggestions);
        btnAddDishes = findViewById(R.id.btnAddDishes);

//        imageViewExample = findViewById(R.id.imageViewExample);
//        imageViewExample.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(NutriMainActivity.this, IngredientsActivity.class);
//                startActivity(i);
//            }
//        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(NutriMainActivity.this);
        rvSuggetions.setLayoutManager(linearLayoutManager);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        intro();
        platilloSels = new ArrayList<>();
        viewModelDBL = ViewModelProviders.of(this).get(AlimentosViewModel.class);
        actPlatillos();
        startClock();
        
//        suggestedDishesList = new ArrayList<>();
//        categoriesFoodPointsList = new ArrayList<>();
//
//        categoriesFoodPointsList.add(new CategoriesFoodPoints(1, "Frutas", "15", 1));
//        categoriesFoodPointsList.add(new CategoriesFoodPoints(2, "Verduras", "24", 2));
//        categoriesFoodPointsList.add(new CategoriesFoodPoints(3, "Cereales", "124", 3));
//        categoriesFoodPointsList.add(new CategoriesFoodPoints(4, "Leguminosas", "94", 4));
//        categoriesFoodPointsList.add(new CategoriesFoodPoints(5, "Carnes", "5", 5));
//        categoriesFoodPointsList.add(new CategoriesFoodPoints(6, "Lacteos", "17", 6));
//        categoriesFoodPointsList.add(new CategoriesFoodPoints(7, "Grasas", "4", 7));
//        categoriesFoodPointsList.add(new CategoriesFoodPoints(8, "Libres", "37", 8));
//
//        for (int i = 1; i < 10; i++) {
//            suggestedDishesList.add(new SuggestedDishes(i, R.drawable.tacos_lechuga, "Tacos de lechuga", "" +
//                    "½ kg de pollo molido orgánico (crudo)\n" +
//                    "⅔ taza de albahaca fresca picada.\n" +
//                    "6 cdasde liquid aminos (sustituto de soya)\n" +
//                    "4 cdas de jugo de limón\n" +
//                    "4 dientes de ajo picados\n" +
//                    "2 chiles cuaresmeño (jalapeños) desvenados y picados\n" +
//                    "1 cda de miel de abeja cruda\n" +
//                    "1 cda de salsa tabasco\n" +
//                    "5 cebollitas de cambray con rabo rebanadas, dejar una aparte\n" +
//                    "2 cdas de aceite de oliva\n" +
//                    "¾ taza de jícama finamente picada\n" +
//                    "8 champiñones picados\n" +
//                    "1 cucharada de jengibre rallado\n" +
//                    "½ taza de manzana pelada y picada\n" +
//                    "½ taza de nuez picada\n" +
//                    "Hojas de lechuga romana",
//                    "1.- Se revuelven el pollo, la albahaca, el liquid aminos, el limón, el ajo, los chiles, la miel, la salsa tabasco y 4 de las 5 cebollitas de cambray. (Esto se puede hacer desde un día antes y dejar en el refrigerador o se puede hacer en el momento.)\n" +
//                            "\n2.- Se ponen las dos cucharadas de aceite de oliva en un sartén. Ya que está caliente el aceite, se agrega el pollo marinado (todo lo que hicimos en el paso 1), y se deja que se dore hasta que el pollo esté 100% cocido. Durante este paso hay que estar revolviendo el pollo para que no queden bolitas.\n" +
//                            "\n3.- Ya que el pollo está cocido se agregan al sartén la jicama, los champiñones, el jengibre y la manzana, y se revuelven bien todos los ingredientes. Se deja un ratito en el sartén para que se incorporen todos los ingredientes (5-6 minutos) y se mantiene caliente.\n" +
//                            "\n4.- Se pone el pollo en un platón y alrededor las hojas de lechuga para que cada quien se prepare sus tacos y se pone en un recipiente por separado, la nuez y la cebollita. Estas se pueden usar como topping de los tacos, al gusto de cada quien.", 100, categoriesFoodPointsList));
//            suggestedDishesList.add(new SuggestedDishes(++i, R.drawable.apple_green, "Manzana", "Fruta entera", "Partirla en trozos", 175, categoriesFoodPointsList));
//        }
//
//        suggetionAdapter = new SuggetionAdapter(NutriMainActivity.this, suggestedDishesList);
//        rvSuggetions.setAdapter(suggetionAdapter);

        btnSeeObjetive.setOnClickListener(v -> {
            Intent intent = new Intent(NutriMainActivity.this, SeeObjetiveUserActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        });

        ivOptions.setOnClickListener(v -> {
            Intent i = new Intent(NutriMainActivity.this, CategoriesActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        });
    }

    private void startClock() {
        mActive = true;
        mHandler.post(mRunnable);
    }

    public void actPlatillos() {
        for (int i = 0; i < 8; i++) {
            dif_objectives1[i] = objectives_food1[i] - objectives_food1D[i] + 10;
        }

        platilloSels.clear();

        viewModelDBL.setPlatillosXGrupoYPuntos(dif_objectives1[0], dif_objectives1[1], dif_objectives1[2], dif_objectives1[3], dif_objectives1[4], dif_objectives1[5], dif_objectives1[6], dif_objectives1[7]);
        viewModelDBL.getPlatillosXGrupoYPuntos().observe(this, new Observer<List<PlatilloPorGrupoYPuntos>>() {
            @Override
            public void onChanged(List<PlatilloPorGrupoYPuntos> platilloPorGrupoYPuntos) {
                for (PlatilloPorGrupoYPuntos p : platilloPorGrupoYPuntos) {
                    platilloSels.add(new PlatilloSel(p.getId(), p.getNombre(), p.getPuntos(), p.getRuta_Imagen()));
                }
                adapter = new SuggetionAdapterDos(NutriMainActivity.this, platilloSels, NutriMainActivity.this::onDishAction);
                rvSuggetions.setAdapter(adapter);
                btnAddDishes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        List<PlatilloSel> dishesSelected = adapter.getSelectedDishes();
                        for (PlatilloSel platillo : dishesSelected) {
                            double id = platillo.getId();
                            viewModelDBL.setPuntosPlatillo(id);
                            viewModelDBL.getPuntosPlatillo().observe(NutriMainActivity.this, new Observer<List<PuntosGrupoPlatillo>>() {
                                @Override
                                public void onChanged(List<PuntosGrupoPlatillo> puntosGrupoPlatillos) {
                                    addPointsObjectives(puntosGrupoPlatillos);
                                }
                            });
                        }
                        Toast.makeText(NutriMainActivity.this, "Objetivos actualizados", Toast.LENGTH_SHORT).show();
                        btnAddDishes.setVisibility(View.GONE);
                        adapter.deSelected();
                        actPlatillos();
                    }
                });
            }
        });
    }

    private void intro() {
        Thread t = new Thread(() -> {
            boolean isIntro = SharedPreferencesManager.getSomeBooleanValue(Constants.MY_INTRO);
            boolean isInfoUser = SharedPreferencesManager.getSomeBooleanValue(Constants.INFO_USER_DATA);

            if (isIntro) {
                Intent i = new Intent(NutriMainActivity.this, MyIntro.class);
                startActivity(i);
                finish();
            } else if (isInfoUser) {
                Intent i = new Intent(NutriMainActivity.this, DataUserActivity.class);
                startActivity(i);
                finish();
            }
        });
        t.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_conf, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent i1 = new Intent(NutriMainActivity.this, ResultsActivity.class);
        Intent i2 = new Intent(NutriMainActivity.this, SettingsActivity.class);

        switch (item.getItemId()) {
            case R.id.mnuResultados:
                startActivity(i1);
                break;
            case R.id.mnuConfiguraciones:
                startActivity(i2);
                break;
            case R.id.mnuActualizar:
                bdLlenar();
                break;
            case R.id.mnuReiniciarO:
                getAllObjetives();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void bdLlenar() {
        viewModelDBL.insertGrupoAlimento(new Grupo_alimento(1, "Frutas", ""));
        viewModelDBL.insertGrupoAlimento(new Grupo_alimento(2, "Verduras", ""));
        viewModelDBL.insertGrupoAlimento(new Grupo_alimento(3, "Cereales", ""));
        viewModelDBL.insertGrupoAlimento(new Grupo_alimento(4, "Leguminosas", ""));
        viewModelDBL.insertGrupoAlimento(new Grupo_alimento(5, "Carnes", ""));
        viewModelDBL.insertGrupoAlimento(new Grupo_alimento(6, "Lacteos", ""));
        viewModelDBL.insertGrupoAlimento(new Grupo_alimento(7, "Grasas", ""));
        viewModelDBL.insertGrupoAlimento(new Grupo_alimento(8, "Libres", ""));


        viewModelDBL.insertTipoPorcion(new Tipo_porcion(1, 1, "Pizca", "Pizca"));
        viewModelDBL.insertTipoPorcion(new Tipo_porcion(2, 1, "Cucharadita", "Cdta"));
        viewModelDBL.insertTipoPorcion(new Tipo_porcion(3, 1, "Cucharada", "Cda"));
        viewModelDBL.insertTipoPorcion(new Tipo_porcion(4, 1, "Taza", "Tz"));
        viewModelDBL.insertTipoPorcion(new Tipo_porcion(5, 1, "Kilogramo", "Kg"));
        viewModelDBL.insertTipoPorcion(new Tipo_porcion(6, 1, "Gramo", "g"));
        viewModelDBL.insertTipoPorcion(new Tipo_porcion(7, 1, "Litro", "l"));
        viewModelDBL.insertTipoPorcion(new Tipo_porcion(8, 1, "Mililitro", "ml"));
        viewModelDBL.insertTipoPorcion(new Tipo_porcion(9, 1, "Pieza", "Pza"));
        viewModelDBL.insertTipoPorcion(new Tipo_porcion(10, 1, "Al gusto", "Al gusto"));


        viewModelDBL.insertAlimento(new Alimento(1, 9, 1, "Ciruela", "", 10, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(2, 9, 1, "Chabacano", "", 7, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(3, 9, 1, "Durazno", "", 15, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(4, 9, 1, "Granada", "", 15, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(5, 9, 1, "Guayaba", "", 15, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(6, 9, 1, "Higo", "", 10, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(7, 9, 1, "Kiwi", "", 20, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(8, 9, 1, "Lima", "", 10, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(9, 9, 1, "Mandarina", "", 15, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(10, 9, 1, "Mango", "", 15, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(11, 9, 1, "Manzana", "", 30, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(12, 9, 1, "Naranja", "", 15, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(13, 9, 1, "Nectarina", "", 30, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(14, 9, 1, "Pasa", "", 3, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(15, 9, 1, "Pera", "", 30, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(16, 9, 1, "Platano", "", 60, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(17, 9, 1, "Tejocote", "", 30, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(18, 9, 1, "Toronja", "", 15, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(19, 9, 1, "Tuna", "", 10, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(20, 9, 1, "Uva", "", 2, "", 1, "1"));

        viewModelDBL.insertAlimento(new Alimento(21, 4, 2, "Apio", "", 15, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(22, 4, 2, "Brócoli", "", 15, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(23, 9, 2, "Calabacita", "", 15, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(24, 4, 2, "Col", "", 30, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(25, 9, 2, "Chayote", "", 30, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(26, 3, 2, "Chicharo", "", 5, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(27, 9, 2, "Esparrago", "", 3, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(28, 4, 2, "Espinaca", "", 7, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(29, 4, 2, "Jícama", "", 30, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(30, 9, 2, "Jitomate", "", 4, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(31, 4, 2, "Lechuga", "", 7, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(32, 4, 2, "Nopal", "", 15, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(33, 4, 2, "Pepino", "", 5, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(34, 4, 2, "Rábano", "", 8, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(35, 9, 2, "Pimiento", "", 15, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(36, 4, 2, "Zanahoria", "", 7, "", 1, "1"));

        viewModelDBL.insertAlimento(new Alimento(37, 4, 3, "Arroz", "", 15, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(38, 4, 3, "Avena integral", "", 105, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(39, 9, 3, "Barrita de avena", "", 70, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(40, 9, 3, "Bolillo integral", "", 105, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(41, 9, 3, "Bollo de hamburguesa", "", 70, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(42, 9, 3, "Camote", "", 140, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(43, 3, 3, "Centeno", "", 7, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(44, 4, 3, "Cereal integral", "", 105, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(45, 9, 3, "Elote", "", 30, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(46, 4, 3, "Pasta para sopa", "", 70, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(47, 9, 3, "Galleta dulce", "", 15, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(48, 9, 3, "Galleta integral", "", 11, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(49, 9, 3, "Galleta salada integral", "", 9, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(50, 4, 3, "Germen de trigo", "", 140, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(51, 3, 3, "Granola baja en grasa", "", 88, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(52, 9, 3, "Pan de hot-dog", "", 70, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(53, 4, 3, "Palomitas naturales", "", 15, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(54, 9, 3, "Pan blanco rebanado", "", 35, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(55, 9, 3, "Pan tostado rebanado", "", 30, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(56, 9, 3, "Papa", "", 23, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(57, 9, 3, "Tortilla de maíz", "", 35, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(58, 9, 3, "Tortilla integral", "", 35, "", 1, "1"));

        viewModelDBL.insertAlimento(new Alimento(59, 4, 4, "Alubia", "", 120, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(60, 4, 4, "Frijol", "", 120, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(61, 4, 4, "Garbanzo", "", 120, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(62, 4, 4, "Haba", "", 240, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(63, 4, 4, "Lentejas", "", 120, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(64, 4, 4, "Soya", "", 180, "", 1, "1"));

        viewModelDBL.insertAlimento(new Alimento(65, 6, 5, "Atún en agua", "", 40, "", 35, "35"));
        viewModelDBL.insertAlimento(new Alimento(66, 6, 5, "Bacaláo", "", 40, "", 45, "45"));
        viewModelDBL.insertAlimento(new Alimento(67, 6, 5, "Bistec", "", 40, "", 35, "35"));
        viewModelDBL.insertAlimento(new Alimento(68, 6, 5, "Carne molida", "", 40, "", 35, "35"));
        viewModelDBL.insertAlimento(new Alimento(69, 9, 5, "Huevo entero", "", 30, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(70, 9, 5, "Clara de huevo", "", 20, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(71, 9, 5, "Chuleta asada", "", 80, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(72, 6, 5, "Filete de pescado", "", 40, "", 45, "45"));
        viewModelDBL.insertAlimento(new Alimento(73, 9, 5, "Jamón", "", 20, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(74, 6, 5, "Filete de pechuga", "", 40, "", 40, "40"));
        viewModelDBL.insertAlimento(new Alimento(75, 9, 5, "Mojarra", "", 120, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(76, 6, 5, "Pollo molido", "", 40, "", 40, "40"));
        viewModelDBL.insertAlimento(new Alimento(77, 9, 5, "Muslo de pollo", "", 120, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(78, 9, 5, "Pechuga de pollo", "", 80, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(79, 6, 5, "Sardina", "", 40, "", 40, "40"));
        viewModelDBL.insertAlimento(new Alimento(80, 6, 5, "Filete de jamón", "", 40, "", 30, "30"));
        viewModelDBL.insertAlimento(new Alimento(81, 6, 5, "Sierra", "", 40, "", 50, "50"));

        viewModelDBL.insertAlimento(new Alimento(82, 4, 6, "Leche descremada", "", 50, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(83, 3, 6, "Leche descremada en polvo", "", 138, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(84, 4, 6, "Leche de soya", "", 50, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(85, 3, 6, "Queso cottage", "", 14, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(86, 6, 6, "Queso de cabra", "", 40, "", 20, "20"));
        viewModelDBL.insertAlimento(new Alimento(87, 6, 6, "Queso panela", "", 40, "", 40, "40"));
        viewModelDBL.insertAlimento(new Alimento(88, 6, 6, "Queso fresco", "", 40, "", 40, "40"));
        viewModelDBL.insertAlimento(new Alimento(89, 6, 6, "Queso Oaxaca", "", 40, "", 40, "40"));
        viewModelDBL.insertAlimento(new Alimento(90, 3, 6, "Requesón", "", 14, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(91, 4, 6, "Yogurt light", "", 60, "", 1, "1"));

        viewModelDBL.insertAlimento(new Alimento(92, 3, 7, "Aceite", "", 200, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(93, 3, 7, "Aceite de canola", "", 200, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(94, 3, 7, "Aceite de girasol", "", 200, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(95, 3, 7, "Aceite de maíz", "", 200, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(96, 3, 7, "Aceite de oliva", "", 200, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(97, 9, 7, "Aceituna negra", "", 5, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(98, 3, 7, "Aderezo", "", 98, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(99, 4, 7, "Aguacate", "", 9, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(100, 9, 7, "Nuez", "", 6, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(101, 9, 7, "Almendra", "", 3, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(102, 9, 7, "Pistache", "", 7, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(103, 6, 7, "Cacahuate", "", 25, "", 20, "20"));
        viewModelDBL.insertAlimento(new Alimento(104, 3, 7, "Pepita con cascara", "", 12, "", 1, "1"));

        viewModelDBL.insertAlimento(new Alimento(105, 4, 8, "Agua natural", "", 0, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(106, 9, 8, "Ajo", "", 0, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(107, 3, 8, "Café descafeinado", "", 0, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(108, 3, 8, "Canela", "", 0, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(109, 3, 8, "Clavo", "", 0, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(110, 9, 8, "Cebolla", "", 0, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(111, 9, 8, "Chile", "", 0, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(112, 4, 8, "Epazote", "", 0, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(113, 4, 8, "Laurel", "", 0, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(114, 9, 8, "Limón", "", 0, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(115, 4, 8, "Hierbabuena", "", 0, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(116, 4, 8, "Manzanilla", "", 0, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(117, 3, 8, "Mostaza", "", 0, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(118, 4, 8, "Orégano", "", 0, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(119, 4, 8, "Perejil", "", 0, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(120, 4, 8, "Alcaparraz", "", 0, "", 1, "1"));
        viewModelDBL.insertAlimento(new Alimento(121, 4, 8, "Anis", "", 0, "", 1, "1"));

        viewModelDBL.insertTipoPlatillo(new Tipo_platillo(1, "Comida"));

        viewModelDBL.insertPlatillo(new Platillo(1, 1, "Ensalada de bacalao", 149, "Paso 1:\nCalentar el horno a 200°c, coloque el pescado " +
                "en un recipiente poco profundo y vierta un poco de jugo de limon sobre el pescado (tambien puede condimentar con un poco de pimienta negra). Hornee durante 8 minutos.\n" +
                "\nPaso 2:\nMientras tanto, ponga el resto de los ingredientes, más un poco de jugo de limon si asi lo desea, en un bol y combinelos bien. vacielo en un plato y coloque " +
                "el bacalao encima, asegurese de cubrirlo bien entre los ingredientes.", 1, ""));
        viewModelDBL.insertPlatillo(new Platillo(2, 1, "Pollo con requeson", 137, "Paso 1:\nHaga un corte profundo a lo largo de la pechuga, ponga " +
                "una cucharada de requeson dentro, posteriormente con la ayudad de un palillo de dientes cierre la apertura de la pechuga\n\nPaso 2:\nColoque la pechuga de pollo sobre una bandeja " +
                "para asar barnizada con una cucharadita de aceite de oliva (tambien puede condimentar con un poco de pimienta negra). Cocine a temperatura media durante 8 minutos.\n\nPaso 3:\n" +
                " Mientras tanto corte la cebolla, el tomate y el ajo y combinelos bien, finalmente incluya la pechuga al plato y combinelo bien entre los ingredientes", 1, ""));
        viewModelDBL.insertPlatillo(new Platillo(3, 1, "Arroz frito", 136, "Paso 1:\nPonga el arroz en una cacerola y agreguele 200 ml de agua hirviendo, " +
                "cocine a fuego lento durante 12 minutos.\n\nPaso 2:\nMientras tanto pique las verduras y corte el jamon en trozitos, fria el jamon en una sarten barnizada con aceite de oliva y " +
                "agregue los demas ingredientes, cocine a fuego medio, despues vierta un huevo batido a la sarten inclinandola un poco para formar un omelette delgado.\n\nPaso 3:\n" +
                "Vacie el arroz cocido a la sarten, corte en tiras el omelette, mezcle bien los ingredientes y fria durante 5 minutos.", 1, ""));


        viewModelDBL.insertIngrediente(new Ingrediente(1, 66, 9, 1, 80, 2, "2"));
        viewModelDBL.insertIngrediente(new Ingrediente(2, 114, 10, 1, 0, 0, ""));
        viewModelDBL.insertIngrediente(new Ingrediente(3, 10, 9, 1, 15, 1, "1"));
        viewModelDBL.insertIngrediente(new Ingrediente(4, 99, 9, 1, 25, 0.33F, "1/3"));
        viewModelDBL.insertIngrediente(new Ingrediente(5, 33, 9, 1, 10, 0.33F, "1/3"));
        viewModelDBL.insertIngrediente(new Ingrediente(6, 30, 9, 1, 4, 1, "1"));
        viewModelDBL.insertIngrediente(new Ingrediente(7, 110, 10, 1, 0, 0, ""));
        viewModelDBL.insertIngrediente(new Ingrediente(8, 31, 9, 1, 15, 0.20F, "1/5"));

        viewModelDBL.insertIngrediente(new Ingrediente(9, 78, 9, 2, 80, 1, "1"));
        viewModelDBL.insertIngrediente(new Ingrediente(10, 90, 3, 2, 10, 1, "1"));
        viewModelDBL.insertIngrediente(new Ingrediente(11, 96, 2, 2, 25, 1, "1"));
        viewModelDBL.insertIngrediente(new Ingrediente(12, 30, 9, 2, 8, 1, "1"));
        viewModelDBL.insertIngrediente(new Ingrediente(13, 110, 9, 2, 10, 0.33F, "1/3"));
        viewModelDBL.insertIngrediente(new Ingrediente(14, 30, 9, 2, 4, 1, "1"));
        viewModelDBL.insertIngrediente(new Ingrediente(15, 106, 9, 2, 0, .25F, "1/4"));

        viewModelDBL.insertIngrediente(new Ingrediente(16, 37, 4, 3, 35, 0.5F, "1/2"));
        viewModelDBL.insertIngrediente(new Ingrediente(17, 96, 2, 3, 25, 1, "1"));
        viewModelDBL.insertIngrediente(new Ingrediente(18, 69, 9, 3, 30, 1, "1"));
        viewModelDBL.insertIngrediente(new Ingrediente(19, 76, 9, 3, 20, 1, "1"));
        viewModelDBL.insertIngrediente(new Ingrediente(20, 36, 9, 3, 6, 1, "1"));
        viewModelDBL.insertIngrediente(new Ingrediente(21, 110, 9, 3, 0, 0.33F, "1/3"));
        viewModelDBL.insertIngrediente(new Ingrediente(22, 106, 9, 3, 0, 0.25F, "1/4"));
        viewModelDBL.insertIngrediente(new Ingrediente(23, 26, 3, 3, 20, 4, "4"));

        viewModelDBL.insertPuntosGrupo(new Punto_grupo(1, 1, 1, 45));
        viewModelDBL.insertPuntosGrupo(new Punto_grupo(2, 2, 1, 29));
        viewModelDBL.insertPuntosGrupo(new Punto_grupo(3, 5, 1, 80));
        viewModelDBL.insertPuntosGrupo(new Punto_grupo(4, 7, 1, 25));

        viewModelDBL.insertPuntosGrupo(new Punto_grupo(5, 2, 2, 12));
        viewModelDBL.insertPuntosGrupo(new Punto_grupo(6, 5, 2, 80));
        viewModelDBL.insertPuntosGrupo(new Punto_grupo(7, 6, 2, 10));
        viewModelDBL.insertPuntosGrupo(new Punto_grupo(8, 7, 2, 25));
        viewModelDBL.insertPuntosGrupo(new Punto_grupo(9, 8, 2, 10));

        viewModelDBL.insertPuntosGrupo(new Punto_grupo(10, 2, 3, 26));
        viewModelDBL.insertPuntosGrupo(new Punto_grupo(11, 3, 3, 35));
        viewModelDBL.insertPuntosGrupo(new Punto_grupo(12, 5, 3, 50));
        viewModelDBL.insertPuntosGrupo(new Punto_grupo(13, 7, 3, 25));


    }

    private void guardarObjetivo(String objetivo, int puntos) {
        SharedPreferencesManager.setSomeIntValue(objetivo, puntos);
    }

    private int obtenerObjetivo(String objetivo) {
        return SharedPreferencesManager.getSomeIntValue(objetivo);
    }

    private void setObjectives() {
        for (int i = 0; i < 8; i++) {
            guardarObjetivo(Constants.OBJETIVE_FOOD1[i], 0);
        }

        for (int i = 0; i < 4; i++) {
            guardarObjetivo(Constants.OBJETIVE_FOOD1[i], 40);
            //guardarObjetivo(Constants.OBJETIVE_FOOD1_DONE[i], (i * 10 + 10));
            guardarObjetivo(Constants.OBJETIVE_FOOD1_DONE[i], 0);
            guardarObjetivo(Constants.OBJETIVE_FOOD1_DONE[i + 4], 0);
        }
    }

    private void getAllObjetives() {
        setObjectives();
        for (int i = 0; i < 8; i++) {
            objectives_food1[i] = obtenerObjetivo(Constants.OBJETIVE_FOOD1[i]);
            objectives_food1D[i] = obtenerObjetivo(Constants.OBJETIVE_FOOD1_DONE[i]);
        }
    }


    @Override
    public void onDishAction(boolean isSelected) {
        if (isSelected) {
            btnAddDishes.setVisibility(View.VISIBLE);
        } else {
            btnAddDishes.setVisibility(View.GONE);
        }
    }

    public void addPointsObjectives(List<PuntosGrupoPlatillo> puntos) {
        for (PuntosGrupoPlatillo p : puntos) {
            guardarObjetivo(Constants.OBJETIVE_FOOD1_DONE[(p.getGrupo() - 1)], p.getPuntos());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        String name = SharedPreferencesManager.getSomeStringValue(Constants.INFO_USER_NAME);
        if (!name.equals("")) {
            tvName.setText("Bienvenido, " + name);
        } else {
            tvName.setText("Bienvenido a NutriApp");
        }
    }
}