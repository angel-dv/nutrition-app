package com.inblexapp.nutrition_app.Activities;

import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.EditTextPreference;
import androidx.preference.ListPreference;
import androidx.preference.PreferenceFragmentCompat;

import com.inblexapp.nutrition_app.R;
import com.inblexapp.nutrition_app.Utils.Constants;
import com.inblexapp.nutrition_app.Utils.SharedPreferencesManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        public static final String TAG = SettingsActivity.class.getSimpleName();
        EditTextPreference etPreferenceName, etPreferenceDate, etPreferenceHeight, etPreferenceWeight;
        ListPreference lpPreferenceGender, lpPreferenceTime;

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
            findPref();
            dataUser();
        }

        private void findPref() {
            lpPreferenceGender = findPreference("gender");
            lpPreferenceTime = findPreference("time");
            etPreferenceName = findPreference("name");
            etPreferenceDate = findPreference("date");
            etPreferenceHeight = findPreference("height");
            etPreferenceWeight = findPreference("weight");

            if (etPreferenceName != null) {
                etPreferenceName.setOnBindEditTextListener(editText -> {
                    editText.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
                    editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                    editText.setSelection(editText.getText().length());
                });
            }

            if (etPreferenceDate != null) {
                etPreferenceDate.setOnBindEditTextListener(editText -> {
                    editText.setInputType(InputType.TYPE_CLASS_DATETIME);
                    editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                    editText.setSelection(editText.getText().length());
                });
            }

            if (etPreferenceHeight != null) {
                etPreferenceHeight.setOnBindEditTextListener(editText -> {
                    editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                    editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                    String height = etPreferenceHeight.getText().toString();
                    String[] allHeight = height.split(" cm");
                    editText.setSelection(allHeight[0].length());
                });
            }

            if (etPreferenceWeight != null) {
                etPreferenceWeight.setOnBindEditTextListener(editText -> {
                    editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                    editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                    String weight = etPreferenceWeight.getText().toString();
                    String[] allWeight = weight.split(" kg");
                    editText.setSelection(allWeight[0].length());
                });
            }

            if (etPreferenceName != null) {
                etPreferenceName.setOnPreferenceChangeListener((preference, newValue) -> {
                    if (newValue.toString().length() > 0) {
                        String newName = newValue.toString();
                        if (!newName.isEmpty()) {
                            etPreferenceName.setText(newName);
                            SharedPreferencesManager.setSomeStringValue(Constants.INFO_USER_NAME, newName);
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                });
            }

            if (etPreferenceDate != null) {
                etPreferenceDate.setOnPreferenceChangeListener((preference, newValue) -> {
                    if (newValue.toString().length() > 0) {
                        String newDate = newValue.toString();

                        if (!newDate.isEmpty()) {
                            if (newDate.length() >= 8 && newDate.length() <= 10) {
                                if (checkDateFormat(newDate)) {
                                    etPreferenceDate.setText(newDate);
                                    SharedPreferencesManager.setSomeStringValue(Constants.INFO_USER_OLD, newDate);
                                    return true;
                                } else {
                                    return false;
                                }

                            } else {
                                return false;
                            }

                        } else {
                            return false;
                        }

                    } else {
                        return false;
                    }
                });
            }

            if (etPreferenceHeight != null) {
                etPreferenceHeight.setOnPreferenceChangeListener((preference, newValue) -> {
                    if (newValue.toString().length() > 0) {
                        Pattern pattern = Pattern.compile("[^0-9]");
                        Matcher matcher = pattern.matcher(newValue.toString());
                        String valueHeight = matcher.replaceAll("");

                        if (valueHeight.equals("")) {
                            return false;
                        } else {
                            int newHeight = Integer.parseInt(valueHeight);
                            if (newHeight >= 90 && newHeight <= 350) {
                                etPreferenceHeight.setText(newHeight + " cm");
                                SharedPreferencesManager.setSomeIntValue(Constants.INFO_USER_HEIGHT, newHeight);
                                return true;
                            } else {
                                return false;
                            }
                        }
                    } else {
                        return false;
                    }
                });
            }

            if (etPreferenceWeight != null) {
                etPreferenceWeight.setOnPreferenceChangeListener((preference, newValue) -> {
                    if (newValue.toString().length() > 0) {
                        Pattern pattern = Pattern.compile("[^0-9]");
                        Matcher matcher = pattern.matcher(newValue.toString());
                        String valueWeight = matcher.replaceAll("");

                        if (valueWeight.equals("")) {
                            return false;
                        } else {
                            int newWeight = Integer.parseInt(valueWeight);
                            if (newWeight >= 20 && newWeight <= 400) {
                                etPreferenceWeight.setText(newWeight + " kg");
                                SharedPreferencesManager.setSomeIntValue(Constants.INFO_USER_WEIGHT, newWeight);
                                return true;
                            } else {
                                return false;
                            }
                        }
                    } else {
                        return false;
                    }
                });
            }

            lpPreferenceGender.setOnPreferenceChangeListener((preference, newValue) -> {
                SharedPreferencesManager.setSomeStringValue(Constants.INFO_USER_GENDER, newValue.toString());
                lpPreferenceGender.setValue(newValue.toString());
                return false;
            });

            lpPreferenceTime.setOnPreferenceChangeListener((preference, newValue) -> {

                if (newValue.toString().equals("24 Horas")) {
                    SharedPreferencesManager.setSomeBooleanValue(Constants.FORMAT_TIME, true);
                    lpPreferenceTime.setValue(newValue.toString());

                } else if (newValue.toString().equals("12 Horas")) {
                    SharedPreferencesManager.setSomeBooleanValue(Constants.FORMAT_TIME, false);
                    lpPreferenceTime.setValue(newValue.toString());
                }

                return false;
            });
        }

        private void dataUser() {
            String gender = SharedPreferencesManager.getSomeStringValue(Constants.INFO_USER_GENDER);
            String name = SharedPreferencesManager.getSomeStringValue(Constants.INFO_USER_NAME);
            String date = SharedPreferencesManager.getSomeStringValue(Constants.INFO_USER_OLD);
            int height = SharedPreferencesManager.getSomeIntValue(Constants.INFO_USER_HEIGHT);
            int weight = SharedPreferencesManager.getSomeIntValue(Constants.INFO_USER_WEIGHT);
            boolean isTypeFormatHour = SharedPreferencesManager.getSomeBooleanValue(Constants.FORMAT_TIME);

            lpPreferenceGender.setValue(gender);
            etPreferenceName.setText(name);
            etPreferenceDate.setText(date);
            etPreferenceHeight.setText(height + " cm");
            etPreferenceWeight.setText(weight + " kg");

            if (isTypeFormatHour) {
                lpPreferenceTime.setValueIndex(0);
            } else {
                lpPreferenceTime.setValueIndex(1);
            }
        }

        private boolean checkDateFormat(String date) {
            if (date == null || !date.matches("^(1[0-9]|0[1-9]|3[0-1]|2[1-9])/(0[1-9]|1[0-2])/[0-9]{4}$"))
                return false;

            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

            try {
                Date auxDate = format.parse(date);
                SimpleDateFormat df = new SimpleDateFormat("yyyy");
                int year = Integer.parseInt(df.format(auxDate));

//                String actualDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
                int yearActual = Integer.parseInt(new SimpleDateFormat("yyyy", Locale.getDefault()).format(new Date()));
                int auxYear = yearActual - 7;
                int restricYear = auxYear - 110;

                if (auxYear >= year && year >= restricYear) {
                    return true;
                } else {
                    return false;
                }

            } catch (ParseException e) {
                return false;
            }
        }
    }
}