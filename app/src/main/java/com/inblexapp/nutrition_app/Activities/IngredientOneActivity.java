package com.inblexapp.nutrition_app.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.inblexapp.nutrition_app.Database.AlimentosViewModel;
import com.inblexapp.nutrition_app.Database.Tables.Alimento;
import com.inblexapp.nutrition_app.R;

public class IngredientOneActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView tvId, tvName, tvPoint, tvProperties;
    private String name, properties;
    private int id, point;
    private Alimento alimento;

    private AlimentosViewModel viewModelDBL;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredient_one);

        toolbar = findViewById(R.id.toolbar);
        tvId = findViewById(R.id.textViewIdOneIngredient);
        tvName = findViewById(R.id.textViewNameOneIngredient);
        tvPoint = findViewById(R.id.textViewPointOneIngredient);
        tvProperties = findViewById(R.id.textViewProperties);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewModelDBL = ViewModelProviders.of(this).get(AlimentosViewModel.class);

        Bundle bundle = getIntent().getExtras();
        id = bundle.getInt("INGREDIENT_ONE_ID");

        viewModelDBL.setAlimentoById(id);
        viewModelDBL.getAlimentoById().observe(this, new Observer<Alimento>() {
            @Override
            public void onChanged(Alimento alimento) {
                name = alimento.getNombre();
                point = alimento.getPuntos();
                properties = alimento.getDescripcion();
                tvId.setText(String.valueOf(id));
                tvName.setText(name);
                tvPoint.setText(String.valueOf(point) + " puntos");
                tvProperties.setText(properties);
            }
        });

        /*name = bundle.getString("INGREDIENT_ONE_NAME");
        point = bundle.getInt("INGREDIENT_ONE_POINT", -1);
        properties = bundle.getString("INGREDIENT_ONE_PROPERTIES");*/

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }
}