package com.inblexapp.nutrition_app.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;

import com.inblexapp.nutrition_app.Adapters.IngredientsAllAdapter;
import com.inblexapp.nutrition_app.Entities.IngredientsAll.IngredientsAll;
import com.inblexapp.nutrition_app.R;

import java.util.ArrayList;

public class IngredientsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView rvAllIngredients;
    private IngredientsAllAdapter adapter;
    private ArrayList<IngredientsAll> ingredientsAllArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredients);

        toolbar = findViewById(R.id.toolbar);
        rvAllIngredients = findViewById(R.id.recyclerViewAllIngredients);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(IngredientsActivity.this);
        rvAllIngredients.setLayoutManager(linearLayoutManager);

        ingredientsAllArrayList = new ArrayList<>();

        for (int i = 1; i <= 12; i++) {
            ingredientsAllArrayList.add(new IngredientsAll(i,  R.drawable.apple_green, "Manzana", "Contiene nutrientes esenciales para -----", "Pasos", 20));
        }

        adapter = new IngredientsAllAdapter(IngredientsActivity.this, ingredientsAllArrayList);
        rvAllIngredients.setAdapter(adapter);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }
}