package com.inblexapp.nutrition_app.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.inblexapp.nutrition_app.Adapters.ObjectivesAdapter;
import com.inblexapp.nutrition_app.Entities.Objectives.Objective;
import com.inblexapp.nutrition_app.R;
import com.inblexapp.nutrition_app.Utils.Constants;
import com.inblexapp.nutrition_app.Utils.SharedPreferencesManager;

import java.util.ArrayList;
import java.util.List;

public class SeeObjetiveUserActivity extends AppCompatActivity {

    Toolbar toolbar;
    Button btnObjetiveComplete;
    CollapsingToolbarLayout ctlColor;
    View vColor;
    RecyclerView rvListObjectives;
    List<Objective> objectivesList;
    ObjectivesAdapter objectivesAdapter;
    TextView tvObjectivesDay;

    private int[] objectives_food1;
    private int[] objectives_food1D;

    private String[] groupsName = new String[]{"Frutas","Verduras","Cereales","Leguminosas","Carnes","Lácteos","Grasas","Libres"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_objetive_user);

        objectives_food1 = new int[8];
        objectives_food1D = new int[8];

        getAllObjetives();

        int suma = 0;
        for (int i = 0; i < 8; i++) {
            suma = suma + objectives_food1D[i];
        }

        objectivesList = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            if(objectives_food1[i] > 0){
                objectivesList.add(new Objective(groupsName[i],objectives_food1[i],objectives_food1D[i]));
            }
        }

        toolbar = findViewById(R.id.toolbar);
        btnObjetiveComplete = findViewById(R.id.buttonCompleteObjetive);
        ctlColor = findViewById(R.id.collapsingToolbarLayoutColor);
        vColor = findViewById(R.id.viewColor);
        tvObjectivesDay = findViewById(R.id.tvObjectivePointsDay);
        rvListObjectives = findViewById(R.id.rvListObjectives);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvListObjectives.setLayoutManager(linearLayoutManager);

        objectivesAdapter = new ObjectivesAdapter(objectivesList,getApplicationContext());
        rvListObjectives.setAdapter(objectivesAdapter);

        tvObjectivesDay.setText(String.valueOf(suma));

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnObjetiveComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SeeObjetiveUserActivity.this, "Dia completado satisfactoriamente", Toast.LENGTH_SHORT).show();
                btnObjetiveComplete.setEnabled(false);
                btnObjetiveComplete.getBackground().setTint(getResources().getColor(R.color.colorPrimary));
                ctlColor.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                ctlColor.setContentScrimColor(getResources().getColor(R.color.colorPrimary));
                vColor.getBackground().setTint(getResources().getColor(R.color.colorPrimary));
                getWindow().setStatusBarColor(ContextCompat.getColor(SeeObjetiveUserActivity.this, R.color.colorPrimary));
                toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    private void getAllObjetives() {
        for (int i = 0; i < 8; i++) {
            objectives_food1[i] = obtenerObjetivo(Constants.OBJETIVE_FOOD1[i]);
            objectives_food1D[i] = obtenerObjetivo(Constants.OBJETIVE_FOOD1_DONE[i]);
        }
    }

    private int obtenerObjetivo(String objetivo) {
        return SharedPreferencesManager.getSomeIntValue(objetivo);
    }
}