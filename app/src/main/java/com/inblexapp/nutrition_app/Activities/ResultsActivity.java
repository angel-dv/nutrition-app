package com.inblexapp.nutrition_app.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.inblexapp.nutrition_app.Activities.FragmentsResults.FragmentsAdapter;
import com.inblexapp.nutrition_app.Activities.FragmentsResults.MonthResults;
import com.inblexapp.nutrition_app.Activities.FragmentsResults.TodayResults;
import com.inblexapp.nutrition_app.Activities.FragmentsResults.WeekResults;
import com.inblexapp.nutrition_app.R;

public class ResultsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Button btnShare;
    private FragmentsAdapter fragmentsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        inicializacion();
        iniciarViewPager();
    }

    private void inicializacion(){
        toolbar = findViewById(R.id.toolbar);
        tabLayout = findViewById(R.id.tabLayoutRes);
        viewPager = findViewById(R.id.viewPagerRes);
        btnShare = findViewById(R.id.buttonShare);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fragmentsAdapter = new FragmentsAdapter(getSupportFragmentManager());

        btnShare.setOnClickListener(v -> {
            Toast.makeText(this, "Compartir a: ", Toast.LENGTH_SHORT).show();
        });
    }

    private void iniciarViewPager(){
        fragmentsAdapter.addFragment(new TodayResults(),"Hoy");
        fragmentsAdapter.addFragment(new WeekResults(), "Semana");
        fragmentsAdapter.addFragment(new MonthResults(), "Mes");

        viewPager.setAdapter(fragmentsAdapter);

        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }
}