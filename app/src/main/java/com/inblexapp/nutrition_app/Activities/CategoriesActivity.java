package com.inblexapp.nutrition_app.Activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.inblexapp.nutrition_app.Adapters.FoodsAdapter;
import com.inblexapp.nutrition_app.Database.AlimentosViewModel;
import com.inblexapp.nutrition_app.Database.Tables.Alimento;
import com.inblexapp.nutrition_app.R;

import java.util.ArrayList;
import java.util.List;

public class CategoriesActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private AlimentosViewModel viewModel;
    private TextView txtTitleCat;
    private ImageView imgPrev, imgNext;
    private RecyclerView rvFoods;
    private EditText edBuscar;
    private CollapsingToolbarLayout ctlColor;
    private FoodsAdapter adapter;
    private ArrayList<Alimento> foodList;
    private View viewUp;
    private int screen = 0;
    private String titles[] = {"Todos", "Frutas", "Verduras", "Cereales", "Leguminosas", "Carnes", "Lácteos", "Grasas", "Libres"};
    private String hints[] = {"Buscar", "Buscar Frutas", "Buscar Verduras", "Buscar Cereales", "Buscar Leguminosas", "Buscar Carnes", "Buscar Lácteos", "Buscar Grasas", "Libres"};
    private int colors[] = {R.color.colorPrimary, R.color.colorPink, R.color.colorLightGreen, R.color.colorYellow, R.color.colorOrange, R.color.colorRed, R.color.colorLightBlue, R.color.colorLightPurple, R.color.colorDarkGreen};

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        toolbar = findViewById(R.id.toolbar);
        rvFoods = findViewById(R.id.recyclerViewFoods);
        edBuscar = findViewById(R.id.edBuscar);
        ctlColor = findViewById(R.id.collapsingToolbarLayoutColor);
        imgPrev = findViewById(R.id.imgPrev);
        imgNext = findViewById(R.id.imgNext);
        txtTitleCat = findViewById(R.id.txtTitleCat);
        viewUp = findViewById(R.id.viewUp);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CategoriesActivity.this);
        rvFoods.setLayoutManager(linearLayoutManager);

        foodList = new ArrayList<>();

        adapter = new FoodsAdapter(CategoriesActivity.this, foodList);
        rvFoods.setAdapter(adapter);

        viewModel = ViewModelProviders.of(this).get(AlimentosViewModel.class);
        viewModel.getAllAlimentos().observe(this, new Observer<List<Alimento>>() {
            @Override
            public void onChanged(List<Alimento> alimentos) {
                adapter.setFoods(alimentos);
            }
        });

        edBuscar.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                String name = edBuscar.getText().toString();
                name = "%" + name + "%";

                if (screen == 0) {
                    viewModel.setAlimentosByName(name);
                    viewModel.getAlimentosByName().observe(CategoriesActivity.this, new Observer<List<Alimento>>() {
                        @Override
                        public void onChanged(List<Alimento> alimentos) {
                            adapter.setFoods(alimentos);
                        }
                    });
                } else {
                    viewModel.setAlimentosByNameAndGrupo(name, screen);
                    viewModel.getAlimentosByNameAndGrupo().observe(CategoriesActivity.this, new Observer<List<Alimento>>() {
                        @Override
                        public void onChanged(List<Alimento> alimentos) {
                            adapter.setFoods(alimentos);
                        }
                    });
                }

                return false;
            }
        });

        imgPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (screen > 0)
                    screen--;
                else
                    screen = 8;

                DataChange();
            }
        });

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (screen < 8)
                    screen++;
                else
                    screen = 0;

                DataChange();
            }
        });

    }

    private void DataChange() {
        txtTitleCat.setText(titles[screen]);
        edBuscar.setHint(hints[screen]);
        viewUp.setBackgroundColor(getResources().getColor(colors[screen]));
        getWindow().setStatusBarColor(ContextCompat.getColor(CategoriesActivity.this, colors[screen]));
        toolbar.setBackgroundColor(getResources().getColor(colors[screen]));
        ctlColor.setBackgroundColor(getResources().getColor(colors[screen]));
        ctlColor.setContentScrimColor(getResources().getColor(colors[screen]));
        if (screen == 0) {
            viewModel.getAllAlimentos().observe(this, new Observer<List<Alimento>>() {
                @Override
                public void onChanged(List<Alimento> alimentos) {
                    adapter.setFoods(alimentos);
                }
            });
        } else {
            viewModel.setAlimentosByGrupo(screen);
            viewModel.getAlimentosByGrupo().observe(CategoriesActivity.this, new Observer<List<Alimento>>() {
                @Override
                public void onChanged(List<Alimento> alimentos) {
                    adapter.setFoods(alimentos);
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

}