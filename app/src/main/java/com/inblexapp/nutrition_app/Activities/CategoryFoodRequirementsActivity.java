package com.inblexapp.nutrition_app.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.inblexapp.nutrition_app.Adapters.IngredientsAdapter;
import com.inblexapp.nutrition_app.Database.AlimentosViewModel;
import com.inblexapp.nutrition_app.Database.Querys.IngredienteCompuesto;
import com.inblexapp.nutrition_app.Entities.ExampleIngredients.Ingredients;
import com.inblexapp.nutrition_app.R;
import com.inblexapp.nutrition_app.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import static android.widget.LinearLayout.VERTICAL;

public class CategoryFoodRequirementsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView rvIngredients;
    private TextView tvCategoryRequirements;
    private CollapsingToolbarLayout ctlColor;
    private View vColor;
    private IngredientsAdapter adapter;
    private List<IngredienteCompuesto> ingredientsList;
    private double idPlatillo;
    private int idGrupo;
    private AlimentosViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_food_requirements);
        viewModel = ViewModelProviders.of(this).get(AlimentosViewModel.class);

        toolbar = findViewById(R.id.toolbar);
        tvCategoryRequirements = findViewById(R.id.textViewCategoryRequirements);
        rvIngredients = findViewById(R.id.recyclerViewIngredients);
        ctlColor = findViewById(R.id.collapsingToolbarLayoutColor);
        vColor = findViewById(R.id.viewColor);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CategoryFoodRequirementsActivity.this);
        rvIngredients.setLayoutManager(linearLayoutManager);
        DividerItemDecoration itemDecor = new DividerItemDecoration(CategoryFoodRequirementsActivity.this, VERTICAL);
        rvIngredients.addItemDecoration(itemDecor);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        receiveExtras();

        viewModel.setIngredientesByGrupo(idPlatillo, idGrupo);
        viewModel.getIngredientesByGrupo().observe(this, new Observer<List<IngredienteCompuesto>>() {
            @Override
            public void onChanged(List<IngredienteCompuesto> ingredienteCompuestos) {
                ingredientsList = ingredienteCompuestos;
                adapter = new IngredientsAdapter(CategoryFoodRequirementsActivity.this, ingredientsList);
                rvIngredients.setAdapter(adapter);
            }
        });


        /*ingredientsList = new ArrayList<>();
        for (int i = 1; i < 12; i++) {
            ingredientsList.add(new Ingredients(i, "Lechuga",  "15 puntos", "1/2", "Aqui va una pequeña descripcion de todo lo que contiene la manzana"));
        }*/


/*
        adapter.setOnItemClickListener(new IngredientsAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(Ingredients ingredients) {
                Toast.makeText(CategoryFoodRequirementsActivity.this,
                        "ID " + ingredients.getId() + " Nombre: " + ingredients.getName(), Toast.LENGTH_SHORT).show();
            }
        });
*/
    }

    private void receiveExtras() {
        String title = getIntent().getStringExtra(Constants.CATEGORY_FOOD_TITLE);
        int color = getIntent().getIntExtra(Constants.CATEGORY_FOOD_COLOR, 0);
        tvCategoryRequirements.setText(title);

        idPlatillo = getIntent().getDoubleExtra(Constants.CATEGORY_FOOD_ID, 1);
        idGrupo = getIntent().getIntExtra(Constants.CATEGORY_GROUP_ID, 1);

        switch (color) {
            case 1:
                ctlColor.setBackgroundColor(getResources().getColor(R.color.colorPink));
                ctlColor.setContentScrimColor(getResources().getColor(R.color.colorPink));
                vColor.getBackground().setTint(getResources().getColor(R.color.colorPink));
                getWindow().setStatusBarColor(ContextCompat.getColor(CategoryFoodRequirementsActivity.this, R.color.colorPink));
                toolbar.setBackgroundColor(getResources().getColor(R.color.colorPink));
                break;
            case 2:
                ctlColor.setBackgroundColor(getResources().getColor(R.color.colorLightGreen));
                ctlColor.setContentScrimColor(getResources().getColor(R.color.colorLightGreen));
                vColor.getBackground().setTint(getResources().getColor(R.color.colorLightGreen));
                getWindow().setStatusBarColor(ContextCompat.getColor(CategoryFoodRequirementsActivity.this, R.color.colorLightGreen));
                toolbar.setBackgroundColor(getResources().getColor(R.color.colorLightGreen));
                break;
            case 3:
                ctlColor.setBackgroundColor(getResources().getColor(R.color.colorYellow));
                ctlColor.setContentScrimColor(getResources().getColor(R.color.colorYellow));
                vColor.getBackground().setTint(getResources().getColor(R.color.colorYellow));
                getWindow().setStatusBarColor(ContextCompat.getColor(CategoryFoodRequirementsActivity.this, R.color.colorYellow));
                toolbar.setBackgroundColor(getResources().getColor(R.color.colorYellow));
                break;
            case 4:
                ctlColor.setBackgroundColor(getResources().getColor(R.color.colorOrange));
                ctlColor.setContentScrimColor(getResources().getColor(R.color.colorOrange));
                vColor.getBackground().setTint(getResources().getColor(R.color.colorOrange));
                getWindow().setStatusBarColor(ContextCompat.getColor(CategoryFoodRequirementsActivity.this, R.color.colorOrange));
                toolbar.setBackgroundColor(getResources().getColor(R.color.colorOrange));
                break;
            case 5:
                ctlColor.setBackgroundColor(getResources().getColor(R.color.colorRed));
                ctlColor.setContentScrimColor(getResources().getColor(R.color.colorRed));
                vColor.getBackground().setTint(getResources().getColor(R.color.colorRed));
                getWindow().setStatusBarColor(ContextCompat.getColor(CategoryFoodRequirementsActivity.this, R.color.colorRed));
                toolbar.setBackgroundColor(getResources().getColor(R.color.colorRed));
                break;
            case 6:
                ctlColor.setBackgroundColor(getResources().getColor(R.color.colorLightBlue));
                ctlColor.setContentScrimColor(getResources().getColor(R.color.colorLightBlue));
                vColor.getBackground().setTint(getResources().getColor(R.color.colorLightBlue));
                getWindow().setStatusBarColor(ContextCompat.getColor(CategoryFoodRequirementsActivity.this, R.color.colorLightBlue));
                toolbar.setBackgroundColor(getResources().getColor(R.color.colorLightBlue));
                break;
            case 7:
                ctlColor.setBackgroundColor(getResources().getColor(R.color.colorLightPurple));
                ctlColor.setContentScrimColor(getResources().getColor(R.color.colorLightPurple));
                vColor.getBackground().setTint(getResources().getColor(R.color.colorLightPurple));
                getWindow().setStatusBarColor(ContextCompat.getColor(CategoryFoodRequirementsActivity.this, R.color.colorLightPurple));
                toolbar.setBackgroundColor(getResources().getColor(R.color.colorLightPurple));
                break;
            case 8:
                ctlColor.setBackgroundColor(getResources().getColor(R.color.colorDarkGreen));
                ctlColor.setContentScrimColor(getResources().getColor(R.color.colorDarkGreen));
                vColor.getBackground().setTint(getResources().getColor(R.color.colorDarkGreen));
                getWindow().setStatusBarColor(ContextCompat.getColor(CategoryFoodRequirementsActivity.this, R.color.colorDarkGreen));
                toolbar.setBackgroundColor(getResources().getColor(R.color.colorDarkGreen));
                break;
            default:
                ctlColor.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                ctlColor.setContentScrimColor(getResources().getColor(R.color.colorAccent));
                vColor.getBackground().setTint(getResources().getColor(R.color.colorAccent));
                getWindow().setStatusBarColor(ContextCompat.getColor(CategoryFoodRequirementsActivity.this, R.color.colorAccent));
                toolbar.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }
}