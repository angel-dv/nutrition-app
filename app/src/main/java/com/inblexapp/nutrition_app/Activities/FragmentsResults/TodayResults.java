package com.inblexapp.nutrition_app.Activities.FragmentsResults;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.inblexapp.nutrition_app.Adapters.ObjetivosDIariosAdapter;
import com.inblexapp.nutrition_app.Entities.Results.Objetivo;
import com.inblexapp.nutrition_app.R;

import java.util.ArrayList;
import java.util.List;

public class TodayResults extends Fragment {

    private ListView listaObjetivos;
    private List<Objetivo> lista;
    private ObjetivosDIariosAdapter adapter;
    private TextView puntos, objetivo, resultado;
    private int totalPuntosDiarios, pObjetivos;
    private String estado;

    public TodayResults() {

    }

    public static TodayResults newInstance() {
        TodayResults fragment = new TodayResults();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        totalPuntosDiarios = 0;
        pObjetivos = 0;

        lista = new ArrayList<>();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_today_results, container, false);
        totalPuntosDiarios = 0;
        pObjetivos = 0;
        estado = "";

        listaObjetivos= v.findViewById(R.id.listaResObjetivos);
        puntos = v.findViewById(R.id.txtResPuntosObtenidos);
        objetivo = v.findViewById(R.id.txtResPuntosBase);
        resultado = v.findViewById(R.id.txtResResultadoDia);

        genLista();
        contarPuntos();
        adapter = new ObjetivosDIariosAdapter(lista,getContext());
        listaObjetivos.setAdapter(adapter);

        puntos.setText(String.valueOf(totalPuntosDiarios));
        String b = " de " + pObjetivos;

        resultado.setTextColor(colorPuntaje(totalPuntosDiarios,pObjetivos));
        resultado.setText(estado);
        objetivo.setText(b);
        return v;
    }

    private void genLista(){
        lista.clear();
        lista.add(new Objetivo("Frutas",20,20));
        lista.add(new Objetivo("Carnes",50,40));
        lista.add(new Objetivo("Cereales",30,35));
        lista.add(new Objetivo("Verduras",20,15));
    }

    private void contarPuntos(){
        int objetivos = lista.size();
        for(int i = 0; i<objetivos; i++){
            totalPuntosDiarios += lista.get(i).getPuntos();
            pObjetivos += lista.get(i).getObjetivo();
        }
    }

    private int colorPuntaje(int puntaje, int objetivo){
        int color = 0;
        if(puntaje == objetivo){
            color = -8339648;
            estado = "Excelente";
        }
        else {
            int dif = 0;
            if(puntaje < objetivo){
                dif = objetivo - puntaje;
            } else {
                dif = puntaje - objetivo;
            }
            if(dif > 19){
                color = -2937068;
                estado = "Terrible";
            }
            else{
                color = -26109;
                estado = "Satisfactorio";
            }

        }
        return color;
    }
}