package com.inblexapp.nutrition_app.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.inblexapp.nutrition_app.R;
import com.inblexapp.nutrition_app.Utils.Constants;
import com.inblexapp.nutrition_app.Utils.SharedPreferencesManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DataUserActivity extends AppCompatActivity {
    Button btnInitMain, btnWomen, btnMen, btnOther;
    EditText etName, etWeight, etHeight;
    TextInputEditText etOld;
    //    TabLayout tlOld, tlWeight;
    //    TabLayout.Tab tabOld, tabWeight;
    int aux;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aaa_exampl_data_user);
        findViews();
        dataUser();
    }

    private void findViews() {
        btnInitMain = findViewById(R.id.buttonNutriMain);
        btnWomen = findViewById(R.id.buttonWomen);
        btnMen = findViewById(R.id.buttonMen);
        btnOther = findViewById(R.id.buttonOther);
        etName = findViewById(R.id.editTextName);
        etOld = findViewById(R.id.editTextOld);
        etWeight = findViewById(R.id.editTextWeight);
        etHeight = findViewById(R.id.editTextHeight);

        etOld.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDateUser();
            }
        });

//        tlOld = findViewById(R.id.tabLayoutOld);
//        tlWeight = findViewById(R.id.tabLayoutWeight);
//        tlOld.setTabGravity(TabLayout.GRAVITY_CENTER);
//        tlWeight.setTabGravity(TabLayout.GRAVITY_CENTER);

//        ArrayList<Integer> old = new ArrayList<>();
//        for (int i = 12; i <= 64; i++) {
//            old.add(i);
//            tlOld.addTab(tlOld.newTab().setText(String.valueOf(i)));
//        }
//
//        int numOld = old.size() / 2;
//        tlOld.getTabAt(numOld).select();
//
//        ArrayList<Integer> weight = new ArrayList<>();
//        for (int i = 30; i <= 220; i++) {
//            weight.add(i);
//            tlWeight.addTab(tlWeight.newTab().setText(String.valueOf(i)));
//        }
//
//        int numWeight = weight.size() / 2;
//        tlWeight.getTabAt(numWeight).select();
    }

    private void setDateUser() {
        Calendar calendar = Calendar.getInstance();
        int yearFirst = 1994;

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar dateUser = Calendar.getInstance();
                int yearCalendar = dateUser.get(Calendar.YEAR);
                int auxYear = yearCalendar - 7;
                int restricYear = auxYear - 110;

                if (auxYear >= year && year >= restricYear) {
                    dateUser.set(year, monthOfYear, dayOfMonth);
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    final Date startDate = dateUser.getTime();
                    String auxDateInfoUser = simpleDateFormat.format(startDate);
//                String date = formatDateInfoUser(auxDateInfoUser);
                    etOld.setText(auxDateInfoUser);
//                SharedPreferencesManager.setSomeStringValue(Constants.INFO_USER_OLD, auxDateInfoUser);
                } else {
                    snackbarDialog();
                }

            }
        }, yearFirst, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void snackbarDialog() {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.coordinatorLayout),
                "Fecha de nacimiento invalida, introduzca una edad mayor a 6 años y menor a 110 años", Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private String formatDateInfoUser(String auxDate) {
        String[] tempDate = auxDate.split("/");
        final int auxMonth = Integer.parseInt(tempDate[1]);
        final String day = tempDate[0];
        String month = "";
        final String year = tempDate[2];

        switch (tempDate[1]) {
            case "01":
                month = "Enero";
                break;
            case "02":
                month = "Febrero";
                break;
            case "03":
                month = "Marzo";
                break;
            case "04":
                month = "Abril";
                break;
            case "05":
                month = "Mayo";
                break;
            case "06":
                month = "Junio";
                break;
            case "07":
                month = "Julio";
                break;
            case "08":
                month = "Agosto";
                break;
            case "09":
                month = "Septiembre";
                break;
            case "10":
                month = "Octubre";
                break;
            case "11":
                month = "Noviembre";
                break;
            case "12":
                month = "Diciembre";
                break;
        }

        String date = day + " de " + month + " de " + year;
        return date;
    }

    private void dataUser() {
        btnWomen.setOnClickListener(v -> {
            aux = 0;
            int women = 1;
            aux = women;
            if (aux == 1) {
                btnWomen.setBackgroundResource(R.drawable.button_background_primary_color);
                btnWomen.setTextColor(getResources().getColor(android.R.color.white));

                btnMen.setBackgroundResource(R.drawable.button_background_circle_purple);
                btnMen.setTextColor(getResources().getColor(android.R.color.black));
                btnOther.setBackgroundResource(R.drawable.button_background_circle_purple);
                btnOther.setTextColor(getResources().getColor(android.R.color.black));
            }
        });

        btnMen.setOnClickListener(v -> {
            aux = 0;
            int men = 2;
            aux = men;
            if (aux == 2) {
                btnMen.setBackgroundResource(R.drawable.button_background_primary_color);
                btnMen.setTextColor(getResources().getColor(android.R.color.white));

                btnWomen.setBackgroundResource(R.drawable.button_background_circle_purple);
                btnWomen.setTextColor(getResources().getColor(android.R.color.black));
                btnOther.setBackgroundResource(R.drawable.button_background_circle_purple);
                btnOther.setTextColor(getResources().getColor(android.R.color.black));
            }
        });

        btnOther.setOnClickListener(v -> {
            aux = 0;
            int other = 3;
            aux = other;
            if (aux == 3) {
                btnOther.setBackgroundResource(R.drawable.button_background_primary_color);
                btnOther.setTextColor(getResources().getColor(android.R.color.white));

                btnWomen.setBackgroundResource(R.drawable.button_background_circle_purple);
                btnWomen.setTextColor(getResources().getColor(android.R.color.black));
                btnMen.setBackgroundResource(R.drawable.button_background_circle_purple);
                btnMen.setTextColor(getResources().getColor(android.R.color.black));
            }
        });

        btnInitMain.setOnClickListener(v -> {
            infoUser();
        });

    }

    private void infoUser() {
        String name = etName.getText().toString();
        String old = etOld.getText().toString();
        String weight = etWeight.getText().toString();
        String height = etHeight.getText().toString();

        if (!weight.isEmpty() && !height.isEmpty()) {
            if (!name.isEmpty()) {
                if (!old.isEmpty()) {
                    int auxHeight = Integer.parseInt(height);
                    if (auxHeight >= 90 && auxHeight <= 350) {
                        int auxWeight = Integer.parseInt(weight);
                        if (auxWeight >= 20 && auxWeight <= 400) {
                            if (aux == 1 || aux == 2 || aux == 3) {
                                goToMain(name, aux, old, auxHeight, auxWeight);
                            } else {
                                Toast.makeText(this, "No ha seleccionado su genero", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            etWeight.setError("Peso no valida");
                        }
                    } else {
                        etHeight.setError("Estatura no valida");
                    }
                } else {
                    etOld.setError("Campo vacio");
                }
            } else {
                etName.setError("Campo vacio");
            }
        } else {
            Toast.makeText(this, "Por favor, llene la información para un mejor plan alimenticio", Toast.LENGTH_SHORT).show();
        }
    }

    private void goToMain(String name, int gender, String old, int height, int weight) {
        String auxGender = "";

        if (gender == 1) {
            auxGender = "Mujer";
        } else if (gender == 2) {
            auxGender = "Hombre";
        } else if (gender == 3) {
            auxGender = "Otro";
        } else {
            auxGender = "No definido";
        }

        SharedPreferencesManager.setSomeStringValue(Constants.INFO_USER_GENDER, auxGender);
        SharedPreferencesManager.setSomeStringValue(Constants.INFO_USER_NAME, name);
        SharedPreferencesManager.setSomeStringValue(Constants.INFO_USER_OLD, old);
        SharedPreferencesManager.setSomeIntValue(Constants.INFO_USER_HEIGHT, height);
        SharedPreferencesManager.setSomeIntValue(Constants.INFO_USER_WEIGHT, weight);

        Intent intent = new Intent(DataUserActivity.this, NutriMainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        SharedPreferencesManager.setSomeBooleanValue(Constants.INFO_USER_DATA, false);
        finish();
    }
}